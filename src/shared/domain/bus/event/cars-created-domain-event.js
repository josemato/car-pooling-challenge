'use strict';

const DomainEvent = require('./domain-event');

/**
 * https://github.com/fmvilas/topic-definition
 */
const FULL_QUALIFIED_EVENT_NAME = 'cabify.cars.1.event.cars.created';

class CarsCreatedDomainEvent extends DomainEvent {
  /**
   * @param {String} id
   * @param {String} eventId
   * @param {String} ocurredOn
   */
  constructor(id, cars, eventId = null, ocurredOn = null) {
    super(id, eventId, ocurredOn);

    this.total = cars.length;
  }

  static eventName() {
    return FULL_QUALIFIED_EVENT_NAME;
  }

  eventName() {
    return FULL_QUALIFIED_EVENT_NAME;
  }

  /**
   * This event is just returning an empty object because we do not to track custom attributes
   * because this event is a wrapper for any Car individual operation
   */
  toPrimitives() {
    return {
      total: this.total,
    };
  }

  getEvent() {
    return {
      eventName: this.eventName(),
      data: this.toPrimitives(),
    };
  }
}

module.exports = CarsCreatedDomainEvent;
