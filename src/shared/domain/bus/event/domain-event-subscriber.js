'use strict';

class DomainEventSubscriber {
  /**
   * returns {Array} events
   */
  static subscribedTo() {
    throw new Error('DomainEventSubscriber subscribedTo needs to be implemented');
  }

  /**
   * @param {DomainEvent} event
   * @param {Object} data
   */
  notify(event, data) {
    throw new Error('DomainEventSubscriber notify needs to be implemented');    
  }
}

module.exports = DomainEventSubscriber;
