'use strict';

const DomainEvent = require('./domain-event');
const DomainEventSubscriber = require('./domain-event-subscriber');

class EventBus {
  /**
   * @param {DomainEvent[]} events
   */
  publish(events) {
    throw new Error('You need to implement publish method');
  }
}

module.exports = EventBus;
