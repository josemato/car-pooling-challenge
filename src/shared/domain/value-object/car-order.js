'use strict';

class CarOrder {
  /**
   * @param {Int} order
   */
  constructor(order) {
    /**
     * For JS, 5.0 is equal to 5 so we need to parse it
     */
    this.order = parseInt(order, 10);
  }

  /**
   * @returns {Int} order
   */
  value() {
    return this.order;
  }
}

module.exports = CarOrder;
