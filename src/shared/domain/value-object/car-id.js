'use strict';

class CarId {
  /**
   * @param {Int} id
   */
  constructor(id) {
    this._ensureIsInteger(id);

    /**
     * For JS, 5.0 is equal to 5 so we need to parse it
     */
    this.id = parseInt(id, 10);
  }

  _ensureIsInteger(id) {
    if (id !== parseInt(id)) {
      throw new TypeError(`car id must be an integer, received: ${id}`);
    }
  }

  /**
   * @returns {Integer} id
   */
  value() {
    return this.id;
  }
}

module.exports = CarId;
