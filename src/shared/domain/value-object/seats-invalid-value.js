'use strict';

class SeatsInvalidValue extends Error {
  constructor(value, min, max) {
    super(`Seat must be an integer between ${min} and ${max}. Received: ${value}`);

    this.name = 'SeatsInvalidValue';
  }
}

module.exports = SeatsInvalidValue;
