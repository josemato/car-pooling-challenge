'use strict';

const AggregateRoot = require('../aggregate-root');
const CarId = require('./car-id');
const Seats = require('./seats');
const CarOrder = require('./car-order');

class Car extends AggregateRoot {
  /**
   * @param {CarId} id
   * @param {Seats} seats
   * @param {CarOrder} order
   */
  constructor(id, seats, order) {
    super();

    this.id = id;
    this.seats = seats;
    this.order = order;
  }

  getId() {
    return this.id;
  }

  getSeats() {
    return this.seats;
  }

  getOrder() {
    return this.order;
  }

  getDto() {
    return {
      id: this.getId().value(),
      seats: this.getSeats().value(),
      order: this.getOrder().value(),
    };
  }

  static fromPrimitives(id, people, order) {
    return new Car(
      new CarId(id),
      new Seats(people),
      new CarOrder(order)
    );
  }
}

module.exports = Car;
