'use strict';

const SeatsInvalidValue = require('./seats-invalid-value');

const SEATS_MIN_PER_CAB = 0;
const SEATS_MAX_PER_CAB = 6;

class Seats {
  /**
   * @param {Int} seats
   */
  constructor(seatsRaw) {
    this._ensureIsInteger(seatsRaw);

    const seats = parseInt(seatsRaw, 10);
    this._ensureIsValidSeatRange(seats);
    /**
     * For JS, 5.0 is equal to 5 so we need to parse it
     */
    this.seats = parseInt(seats, 10);
  }

  _ensureIsInteger(seats) {
    if (seats !== parseInt(seats)) {
      throw new SeatsInvalidValue(seats, SEATS_MIN_PER_CAB, SEATS_MAX_PER_CAB);
    }
  }

  _ensureIsValidSeatRange(seats) {
    /**
     * A seat must be between 4 and 6:
     * Cars have a different amount of seats available,
     * they can accommodate groups of up to 4, 5 or 6 people.
     * More info: https://gitlab.com/cabify-challenge/car-pooling-challenge-josemato#car-pooling-service-challenge
     */
    if (seats < SEATS_MIN_PER_CAB || seats > SEATS_MAX_PER_CAB) {
      throw new SeatsInvalidValue(seats, SEATS_MIN_PER_CAB, SEATS_MAX_PER_CAB);
    }
  }

  /**
   * @returns {Integer} seats
   */
  value() {
    return this.seats;
  }

  /**
   * @param {Seats} seats 
   */
  isEqual(seats) {
    return this.seats === seats.value();
  }

  static minSeatsPerCab() {
    return SEATS_MIN_PER_CAB;
  }

  static maxSeatsPerCab() {
    return SEATS_MAX_PER_CAB;
  }
}

module.exports = Seats;
