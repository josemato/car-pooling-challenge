'use strict';

const CarOder = require('../domain/value-object/car-order-generator');

class CarOrderTimeBased extends CarOder {
  generate() {
    return process.hrtime.bigint();
  }
}

module.exports = CarOrderTimeBased;
