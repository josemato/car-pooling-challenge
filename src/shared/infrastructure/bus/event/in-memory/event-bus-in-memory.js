'use strict';

const Hertzy = require('hertzy');
const EventBus = require('../../../../domain/bus/event/event-bus');
const DomainEvent = require('../../../../domain/bus/event/domain-event');
const CarsCreatedDomainEvent = require('../../../../domain/bus/event/cars-created-domain-event');
const GroupWaitingDomainEvent = require('../../../../../journeys/domain/events/group-waiting-domain-event');
const GroupWaitingDropOffDomainEvent = require('../../../../../journeys/domain/events/group-waiting-drop-off-domain-event');
const GroupRidingDropOffDomainEvent = require('../../../../../journeys/domain/events/group-riding-drop-off-domain-event');
const CarReleasedDomainEvent = require('../../../../../journeys/domain/events/car-released-domain-event');
const CarAvailableDomainEvent = require('../../../../../journeys/domain/events/car-available-domain-event');

const validEventNames = [];

class EventBusInMemory extends EventBus {
  #logger = null;
  #frequency = null;

  /**
   * @param {Logger} logger 
   * @param {Array.<DomainEvent>} subscribers 
   */
  constructor(logger, subscribers = []) {
    super();
    this.#logger = logger;
    this.#frequency = Hertzy.tune('cabify');

    /**
     * For each subscriber, get subscriptions (subscriber.subscribedTo) and
     * attach the executor
     * This feature is disabled :( let's do it manually for this version :))
     * To enable it in a future, pass al domain events by tags on services.yaml
     */
    subscribers.forEach((subscriber) => {
      logger.info('attach_subscriber', subscriber.execute, subscriber.subscribedTo());
      subscriber.subscribedTo().forEach(eventName => this.#frequency.on(eventName, subscriber.execute));
    });

    validEventNames.push(...[
      CarsCreatedDomainEvent.eventName(),
      GroupWaitingDomainEvent.eventName(),
      GroupWaitingDropOffDomainEvent.eventName(),
      GroupRidingDropOffDomainEvent.eventName(),

      CarReleasedDomainEvent.eventName(),
      CarAvailableDomainEvent.eventName(),
    ]);
  }

  subscribeTo(eventName, callback) {
    /**
     * This is useful specially during development in case we forget about connecting to the topic
     * TODO: Using dependency injection, create topics and connect subscribers dynamically
     */
    if (!validEventNames.includes(eventName)) {
      throw new Error(`Event name is not registered: ${eventName}`);
    }

    this.#frequency.on(eventName, callback);
  }

  /**
   * @param {DomainEvent[]} events
   */
  publish(events) {
    try {
      events.forEach((event) => this.#frequency.emit(event.eventName(), event.getEvent()));
    } catch (e) {
      this.#logger.error('Error on publish');
      this.#logger.error(e);
    }
  }
}

module.exports = EventBusInMemory;
