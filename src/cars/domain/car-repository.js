'use strict';

/**
 * In JS there is no interfaces
 * This template is simulating an interface using js classes
 */
class CarRepository {
  /**
   * 
   * @param {Array<Cars>} cars
   */
  addBulk(cars) {
    throw new Error('This method must be implemented');
  }

  getCars() {
    throw new Error('This method must be implemented');
  }

  getAt(index) {
    throw new Error('This method must be implemented');
  }

  getSize() {
    throw new Error('This method must be implemented');
  }
}

module.exports = CarRepository;
