'use strict';

const Car = require('../../shared/domain/value-object/car');
const CarsCreatedDomainEvent = require('../../shared/domain/bus/event/cars-created-domain-event');
const AggregateRoot = require('../../shared/domain/aggregate-root');
const Uuid = require('../../shared/domain/value-object/uuid');

class CarsCollection extends AggregateRoot {
  #id = null;
  #cars = [];

  /**
   * @param {Array<Car} cars
   * @returns {Array<Car>}
   */
  constructor(cars) {
    super();
    /**
     * Uuid should be generated outside this AggregateRoot but this Cars class is just a wrapper to enclose
     * a group of cars
     * In the other hand, uuid should be part of infrastructure but to simplify things I am using it directly, if not,
     * a CarsId value object extending from a Uuid class could be created
     * To avoid looking trough the code, this id can be used to match correlation events if it is necessary
     */
    this.#id = Uuid.random();
    this.#cars.push(...cars);
  }

  getId() {
    return this.#id.value();
  }

  getCars() {
    return this.#cars;
  }

  /**
   * @param {Array<Car>} cars
   * @returns {CarsCollection}
   */
  static create(cars) {
    const carsCollection = new CarsCollection(cars);

    carsCollection.registerEvent(new CarsCreatedDomainEvent(carsCollection.getId(), carsCollection.getCars()));

    return carsCollection;
  }
}

module.exports = CarsCollection;
