'use strict';

const Car = require('../../../shared/domain/value-object/car');
const CarsCollection = require('../../domain/cars');
const CarOrderGenerator = require('../../../shared/domain/value-object/car-order-generator');
const CarId = require('../../../shared/domain/value-object/car-id');
const Seats = require('../../../shared/domain/value-object/seats');
const CarOrder = require('../../../shared/domain/value-object/car-order');

class CarsCreator {
  #repository = null;
  #carOrderGenerator = null;
  #bus = null;
  #logger = null;

  /**
   * @param {CarsRepository} repository
   * @param {CarOrderGenerator} CarOrderGenerator
   * @param {EventBus} bus
   * @param {Logger} logger
   */
  constructor(repository, carOrderGenerator, bus, logger) {
    this.#repository = repository;
    this.#carOrderGenerator = carOrderGenerator;
    this.#bus = bus;
    this.#logger = logger;
  }

  async execute(carsDto) {
    const cars = carsDto.map((carDto) => {
      const { id, seats } = carDto;

      const carId = new CarId(id);
      const carSeats = new Seats(seats);
      const carOrder = new CarOrder(this.#carOrderGenerator.generate());

      return new Car(carId, carSeats, carOrder);
    });

    const carsCollection = CarsCollection.create(cars);

    this.#repository.addBulk(cars);

    this.#bus.publish(carsCollection.pullDomainEvents());
  }
}

module.exports = CarsCreator;
