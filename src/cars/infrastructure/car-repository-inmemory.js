'use strict';

const CarRepository = require('../domain/car-repository');
const CarsCollection = require('../domain/cars');

class CarRepositoryInMemory extends CarRepository {
  /**
   * @property {CarsCollection}
   */
  #carsInMemory = [];

  constructor() {
    super();
  }

  /**
   * @param {Array.<Car>} cars
   */
  addBulk(cars) {
    this.#carsInMemory.length = 0;
    this.#carsInMemory.push(...cars);
  }

  /**
   * returns {CarsCollection}
   */
  getCars() {
    return this.#carsInMemory;
  }

  getAt(index) {
    return this.#carsInMemory[index];
  }

  getSize() {
    return this.#carsInMemory.length;
  }
}

module.exports = CarRepositoryInMemory;
