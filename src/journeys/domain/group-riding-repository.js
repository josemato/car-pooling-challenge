'use strict';

const Car = require('../../shared/domain/value-object/car');
const Group = require('./group');

/**
 * In JS there is no interfaces
 * This template is simulating an interface using js classes
 * 
 * This repository will be helpful to answer questions like:
 *  - Is a group in a ride
 *  - Dropoff group from riding
 *  - Add group into a car (associate a group with a car)
 */
class GroupRidingRepository {
  /**
   * @param {Group} group
   * @param {Car} car
   */
  addGroupIntoAcar(group, car) {
    throw new Error('This method must be implemented');
  }

  /**
   * @param {GroupId} id
   * @return {Car} car
   */
  getRidingGroup(id) {
    throw new Error('This method must be implemented');
  }

  /**
   * @param {GroupId} id
   */
  dropOff(id) {
    throw new Error('This method must be implemented');
  }

  clear() {
    throw new Error('This method must be implemented');
  }

  /**
   * Return number of people who is riding
   */
  size() {
    throw new Error('This method must be implemented');
  }
}

module.exports = GroupRidingRepository;
