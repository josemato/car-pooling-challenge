'use strict';

const GroupIdInvalidValue = require('./group-id-invalid-value');

class GroupId {
  /**
   * @param {Int} id
   */
  constructor(id) {
    this._ensureIsInteger(id);

    /**
     * For JS, 5.0 is equal to 5 so we need to parse it
     */
    this.id = parseInt(id, 10);
  }

  _ensureIsInteger(id) {
    if (id != parseInt(id)) {
      throw new GroupIdInvalidValue(id);
    }
  }

  /**
   * @returns {Integer} id
   */
  value() {
    return this.id;
  }
}

module.exports = GroupId;
