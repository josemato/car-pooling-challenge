'use strict';

class PeopleInvalidValue extends Error {
  constructor(value) {
    super(`People must be an integer between 1 to 6. Received: ${value}`);

    this.name = 'PeopleInvalidValue';
  }
}

module.exports = PeopleInvalidValue;
