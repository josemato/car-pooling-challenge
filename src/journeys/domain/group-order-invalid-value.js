'use strict';

class GroupOrderInvalidValue extends Error {
  constructor(value) {
    super(`Order must be an integer. Received: ${value}`);

    this.name = 'GroupOrderInvalidValue';
  }
}

module.exports = GroupOrderInvalidValue;
