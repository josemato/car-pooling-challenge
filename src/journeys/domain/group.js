'use strict';

const AggregateRoot = require('../../shared/domain/aggregate-root');
const GroupWaitingCreatedDomainEvent = require('./events/group-waiting-domain-event');
const GroupId = require('./group-id');
const GroupOrder = require('./group-order');
const People = require('./people');

const MAX_WAITING_GROUPS = 100000;

class Group extends AggregateRoot {
  /**
   * @param {GroupId} id
   * @param {People} people
   */
  constructor(id, people, order) {
    super();

    this.id = id;
    this.people = people;
    this.order = order;
  }

  getId() {
    return this.id;
  }

  getPeople() {
    return this.people;
  }

  getOrder() {
    return this.order;
  }

  /**
   * Return a base object to simplify development avoiding creating a serialization wrapper
   */
  getDto() {
    return {
      id: this.id.value(),
      people: this.people.value(),
      order: this.order.value(),
    };
  }

  /**
   * @param {GroupId} id
   * @returns {People} people
   */
  static create(id, people, order) {
    const group = new Group(id, people, order);

    group.registerEvent(new GroupWaitingCreatedDomainEvent(group.getId(), group.getPeople(), group.getOrder()));

    return group;
  }

  static maxWaitingGroups() {
    return MAX_WAITING_GROUPS;
  }

  static fromPrimitives(idRaw, peopleRaw, orderRaw) {
    return new Group(
      new GroupId(idRaw),
      new People(peopleRaw),
      new GroupOrder(orderRaw)
    );
  }
}

module.exports = Group;
