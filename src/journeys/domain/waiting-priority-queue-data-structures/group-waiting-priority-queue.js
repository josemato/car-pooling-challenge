'use strict';

const Group = require('../group');

class GroupWaitingPriorityQueue {
  /**
   * @param {Group} group
   */
  addGroup(group) {
    throw new Error('You need to implement your version of GroupWaitingQueue addGroup method');
  }

  /**
   * @param {Group} group
   */
  removeGroup(group) {
    throw new Error('You need to implement your version of GroupWaitingQueue removeGroup method');
  }

  /**
   * @returns {Int} total Number of waiting cars
   */
  size() {
    throw new Error('You need to implement your version of GroupWaitingQueue size method');    
  }

  /**
   * Find a group keeping in mind the priority order where order is the indexed key
   * @param {Group} group
   */
  findGroup(group) {
    throw new Error('You need to implement your version of GroupWaitingQueue findGroup method');
  }

   /**
   * Group position in it's internal priority queue
   * @param {Int} group
   */
  findGroupPosition(group) {
    throw new Error('You need to implement your version of GroupWaitingQueue findGroupPosition method');
  }

  /**
   * @param {People} people 
   * @returns {GroupDto} group
   */
  getFirstGroupByPeople(people) {
    throw new Error('You need to implement your version of GroupWaitingQueue findGroupPosition method');
  }
}

module.exports = GroupWaitingPriorityQueue;
