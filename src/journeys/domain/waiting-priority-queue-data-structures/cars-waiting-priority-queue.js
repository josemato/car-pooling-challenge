'use strict';

const Car = require('../../../shared/domain/value-object/car');
const Seats = require('../../../shared/domain/value-object/seats');

class CarsWaitingPriorityQueue {
  /**
   * @param {Car} car
   */
  addCar(car) {
    throw new Error('This method needs to be implemented');
  }

  /**
   * @param {Seats} seats
   * @returns {Car}
   */
  getFirstCarBySeats(seats) {
    throw new Error('This method needs to be implemented');
  }

  /**
   * Remove all fleet
   */
  clear() {
    throw new Error('This method needs to be implemented');
  }

  /**
   * Given a car and a group, assign a free car to a group
   * @param {Car} car
   * @param {Group} group
   * @returns {Car} assignedCar
   */
  assignCarForAGroup(car, group) {
    throw new Error('This method needs to be implemented');
  }

  /**
   * After a drop off, restore car into the proper queue
   * @param {Car} car
   * @param {Group} group
   */
  restoreCar(car, group) {
    throw new Error('This method needs to be implemented');
  }

  /**
   * @returns {Int} total Number of available cars
   */
  size() {
    throw new Error('This method needs to be implemented');
  }
}

module.exports = CarsWaitingPriorityQueue;
