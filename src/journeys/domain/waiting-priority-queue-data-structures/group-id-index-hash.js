'use strict';

const Group = require('../group');
const GroupId = require('../group-id');

class GroupIdIndexHash {
  #index = null;
  /**
   * Data structure to store group data by group id. We can have 1..100,000 groups
   *  [group id]: [group data]
   * Example:
   * groupId1: [<group1>, <group5>]
   * groupId2: [<group4>, <group2>]
   * groupId3: [<group10>, <group5>]
   * groupId4: [<group45>, <group2>]
   * groupId5: [<group15>, <group21>]
   * groupId6: [<group23>, <group35>]
   * 
   * This structure is optimized for:
   *  - Locate: Check if a group exist
   *  - Dropoff: Remove element
   */

  /**
   * @param {Group} group
   */
  addGroup(group) {
    throw new Error('You need to implement your version of GroupIdIndexHash addGroup method');
  }

  /**
   * @param {GroupId} id
   * @returns {GroupDto} groupDto
   */
  get(id) {
    throw new Error('You need to implement your version of GroupIdIndexHash get method');
  }

  /**
   * @param {GroupId} groupId
   * @eturns {Boolean} deleted
   */
  removeGroup(id) {
    throw new Error('You need to implement your version of GroupIdIndexHash removeGroup method');
  }

  isEmpty() {
    throw new Error('You need to implement your version of GroupIdIndexHash isEmpty method');
  }
}

module.exports = GroupIdIndexHash;
