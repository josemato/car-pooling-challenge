'use strict';

const Group = require('../group');
const GroupId = require('../group-id');

class GroupIdCarIndexHash {
  /**
   * Data structure to store car data by group id
   *  [group id]: [group data]
   * Example:
   * groupId1: <car1>
   * groupId2: <car4>
   * groupId3: <car10>
   * groupId4: <car45>
   * groupId5: <car15>
   * groupIdN: <car23>
   *
   * This structure is optimized for:
   *  - Locate: Check if a group exist
   *  - Dropoff: Remove element
   */

  /**
   * @param {Group} group
   * @param {Car} car
   */
  addGroup(group, car) {
    throw new Error('This method needs to be implemented');
  }

  /**
   * @param {GroupId} id
   * @returns {CarDto} carDto
   */
  get(id) {
    throw new Error('This method needs to be implemented');
  }

  /**
   * @param {GroupId} groupId
   */
  removeGroup(id) {
    throw new Error('This method needs to be implemented');
  }

  /**
   * @returns {Boolean}
   */
  isEmpty() {
    throw new Error('This method needs to be implemented');
  }

  /**
   * returns {Int}
   */
  size() {
    throw new Error('This method needs to be implemented');
  }

  clear() {
    throw new Error('This method needs to be implemented');
  }
}

module.exports = GroupIdCarIndexHash;
