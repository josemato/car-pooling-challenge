'use strict';

const Car = require('../../shared/domain/value-object/car');

class CarsAvailableRepository {
  /**
   * @param {Car} car
   */
  addCar(car) {
    throw new Error('You need to implement your version of CarWaitingQueue addCar method');
  }

  /** 
   * @param {CarId} carId
   */
  getOriginalCarById(carId) {
    throw new Error('You need to implement your version of CarWaitingQueue getOriginalCarById method');
  }

  /**
   * Remove current indexed cars fleet and generate a new index with the current ones
   * @param {Array.<Car>} cars
   */
  generateIndex(cars) {
    throw new Error('You need to implement your version of CarWaitingQueue generateIndex method');
  }

  /**
   * Check if there is some available car on the fleet for the given people number
   * People can be between 1..6 but cars are from 4..6 so let's see if there is a cabify
   * to accomodate the incoming group
   * Optimized to answer with the car with less available seats to suit the group
   * @param {People} people
   * @returns {Car} car
   */
  isThereAvailableCarForGroup(people) {
    throw new Error('You need to implement your version of CarWaitingQueue isThereAvailableCarForGroup method');
  }

  /**
   * Given a group, look for the best car available and assign it to it
   * @param {Group} group 
   * @returns {Car} assignedCar
   */
  assignCarForAGroup(group) {
    throw new Error('You need to implement your version of CarWaitingQueue assignCarForAGroup method');
  }

  /**
   * Dropoff action was done so car needs to add the available seats
   * @param {carId} carId
   * @param {Group} fromGroup
   */
  restoreCar(car, fromGroup) {
    throw new Error('You need to implement your version of CarWaitingQueue restoreCar method');
  }

  /**
   * @returns {Int} total Number of waiting cars
   */
  size() {
    throw new Error('You need to implement your version of CarWaitingQueue size method'); 
  }
}

module.exports = CarsAvailableRepository;
