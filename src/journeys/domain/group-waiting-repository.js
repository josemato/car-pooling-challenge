'use strict';

const Group = require('./group');

/**
 * In JS there is no interfaces
 * This template is simulating an interface using js classes
 * 
 * This repository will be helpful to answer questions like:
 *  - Is a group waiting for a car
 *  - Dropoff group from waiting queue
 *  - Add group into a waiting queue to get a car
 */
class GroupWaitingRepository {
  /**
   * @param {Group} group
   */
  addGroupToQueue(group) {
    throw new Error('This method must be implemented');
  }

  /**
   * @param {GroupId} id
   * @returns {Group} group
   */
  getGroupWaiting(id) {
    throw new Error('This method must be implemented');
  }

  /**
   * @param {GroupId} id
   * @returns {Group} group
   */
  dropOff(id) {
    throw new Error('This method must be implemented');
  }

  /**
   * For any group with people from 1..6, get the first group of each people data
   * @returns {Array.<Group>}
   */
  getOrderedGroupWaitingForCarPerSeat() {
    throw new Error('This method must be implemented');
  }

  clear() {
    throw new Error('This method must be implemented');
  }

  size() {
    throw new Error('This method must be implemented');
  }
}

module.exports = GroupWaitingRepository;
