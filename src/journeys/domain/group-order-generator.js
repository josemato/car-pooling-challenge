'use strict';

class GroupOrderGenerator {
  generate() {
    throw new Error('You need to implement generate method on your class');
  }
}

module.exports = GroupOrderGenerator;
