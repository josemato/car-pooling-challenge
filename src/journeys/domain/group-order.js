'use strict';

const GroupOrderInvalidValue = require('./group-order-invalid-value');

class GroupOrder {
  /**
   * @param {Int} order
   */
  constructor(order) {
    // this._ensureIsInteger(order);

    /**
     * For JS, 5.0 is equal to 5 so we need to parse it
     */
    this.order = parseInt(order, 10);
  }

  _ensureIsInteger(order) {
    if (order != parseInt(order)) {
      throw new GroupOrderInvalidValue(`GroupOrder needs to be an integer. Received: ${order}`);
    }
  }

  /**
   * @returns {Int} order
   */
  value() {
    return this.order;
  }
}

module.exports = GroupOrder;
