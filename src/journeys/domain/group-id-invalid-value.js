'use strict';

class GroupIdInvalidValue extends Error {
  constructor(value) {
    super(`Id must be an integer. Received: ${value}`);

    this.name = 'GroupIdInvalidValue';
  }
}

module.exports = GroupIdInvalidValue;
