'use strict';

const GroupId = require('./group-id');

class GroupNotFound extends Error {
  /**
   * @param {GroupId} id
   */
  constructor(id) {
    super(`Group not found. Received: ${id.value()}`);

    this.name = 'GroupNotFound';
  }
}

module.exports = GroupNotFound;
