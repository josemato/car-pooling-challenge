'use strict';

const DomainEvent = require('../../../shared/domain/bus/event/domain-event');

/**
 * https://github.com/fmvilas/topic-definition
 */
const FULL_QUALIFIED_EVENT_NAME = 'cabify.journey.1.event.car_availability.available';

class CarAvailableDomainEvent extends DomainEvent {
  constructor(id, eventId = null, ocurredOn = null) {
    super(id, eventId, ocurredOn);

    this.id = id;
  }

  static eventName() {
    return FULL_QUALIFIED_EVENT_NAME;
  }

  eventName() {
    return FULL_QUALIFIED_EVENT_NAME;
  }

  getEvent() {
    return {
      eventName: this.eventName(),
      data: this.toPrimitives(),
    };
  }

  toPrimitives() {
    return {
      id: this.id,
    };
  }
}

module.exports = CarAvailableDomainEvent;
