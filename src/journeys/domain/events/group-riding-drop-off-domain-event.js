'use strict';

const DomainEvent = require('../../../shared/domain/bus/event/domain-event');

/**
 * https://github.com/fmvilas/topic-definition
 */
const FULL_QUALIFIED_EVENT_NAME = 'cabify.journey.1.event.group.riding_drop_off';

class GroupRidingDropOffDomainEvent extends DomainEvent {
  constructor(id, peopleId, people, eventId = null, ocurredOn = null) {
    super(id, eventId, ocurredOn);

    this.id = id;
    this.peopleId = peopleId;
    this.people = people;
  }

  static eventName() {
    return FULL_QUALIFIED_EVENT_NAME;
  }

  eventName() {
    return FULL_QUALIFIED_EVENT_NAME;
  }

  getEvent() {
    return {
      eventName: this.eventName(),
      data: this.toPrimitives(),
    };
  }

  toPrimitives() {
    return {
      car: {
        id: this.id,
      },
      group: {
        id: this.peopleId,
        people: this.people,
      }
    };
  }
}

module.exports = GroupRidingDropOffDomainEvent;
