'use strict';

const DomainEvent = require('../../../shared/domain/bus/event/domain-event');

/**
 * https://github.com/fmvilas/topic-definition
 */
const FULL_QUALIFIED_EVENT_NAME = 'cabify.journey.1.event.group.waiting_drop_off';

class GroupWaitingDropOffDomainEvent extends DomainEvent {
  constructor(id, people, eventId = null, ocurredOn = null) {
    super(id, eventId, ocurredOn);

    this.id = id;
    this.people = people;
  }

  static eventName() {
    return FULL_QUALIFIED_EVENT_NAME;
  }

  eventName() {
    return FULL_QUALIFIED_EVENT_NAME;
  }

  getEvent() {
    return {
      eventName: this.eventName(),
      data: this.toPrimitives(),
    };
  }

  toPrimitives() {
    return {
      id: this.id,
      people: this.people,
    };
  }
}

module.exports = GroupWaitingDropOffDomainEvent;
