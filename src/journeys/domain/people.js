'use strict';

const PeopleInvalidValue = require('./people-invalid-value');

const PERSONS_MIN_PER_CAB = 1;
const PERSONS_MAX_PER_CAB = 6;

class People {
  /**
   * @param {Int} peopleRaw
   */
  constructor(peopleRaw) {
    this._ensureIsInteger(peopleRaw);

    const people = parseInt(peopleRaw, 10);
    this._ensureIsValidPeopleRange(people);
    /**
     * For JS, 5.0 is equal to 5 so we need to parse it
     */
    this.people = parseInt(people, 10);
  }

  _ensureIsInteger(people) {
    if (people !== parseInt(people)) {
      throw new PeopleInvalidValue(people);
    }
  }

  _ensureIsValidPeopleRange(people) {
    /**
     * A group of people must be between 1 and 6:
     * People requests cars in groups of 1 to 6.
     * More info: https://gitlab.com/cabify-challenge/car-pooling-challenge-josemato#car-pooling-service-challenge
     */
    if (people < PERSONS_MIN_PER_CAB || people > PERSONS_MAX_PER_CAB) {
      throw new PeopleInvalidValue(people);
    }
  }

  /**
   * @returns {Integer} people
   */
  value() {
    return this.people;
  }

  static minPeoplePerCab() {
    return PERSONS_MIN_PER_CAB;
  }

  static maxPeoplePerCab() {
    return PERSONS_MAX_PER_CAB;
  }
}

module.exports = People;
