'use strict';

const CarReleasedDomainEvent = require('../../domain/events/car-released-domain-event');
const Fastq = require('fastq');

const WORKERS_CONCURRENCY = 1;

const defaultCallbackAfterProcessingEvent = (err => {
  if (err) {
    logger.error(err);
  }
});

/**
 * TODO: This Journey dispatcher is touching too many repositories
 * A good improvement could be to book the car using groupWaitingRepository and
 * carsAvailableRepository and trigger and event, after that, another app services
 * would perform the missing actions: add user into riding queue and remove it from waiting queue
 * but for the purpose of this test I don't know if it's something expected due to other complexity
 * problems like eventual consistency for example
 */
class JourneyDispatcher {
  /**
   * TODO: Move this queue implementation to infrastructure
   * Due I am running out of time I will implement it here
   */
  #parallelQueue = null;

  /**
   * @property {Logger} logger
   */
  #logger = null;

  /**
   * @property {EventBus}
   */
  #bus = null;

  /**
   * @property {groupWaitingRepository}
   */
  #groupWaitingRepository = null;

  /**
   * @property {carsAvailableRepository}
   */
  #carsAvailableRepository = null;

  /**
   * @property {groupRidingRepository}
   */
  #groupRidingRepository = null;

  /**
   * 
   */
  constructor(logger, bus, groupWaitingRepository, carsAvailableRepository, groupRidingRepository) {
    this.#parallelQueue = Fastq(this, this.createWorker(), WORKERS_CONCURRENCY);

    this.#logger = logger;
    this.#bus = bus;
    this.#groupWaitingRepository = groupWaitingRepository;
    this.#carsAvailableRepository = carsAvailableRepository;
    this.#groupRidingRepository = groupRidingRepository;
  }

  createWorker() {
    return (event, callback) => {
      /**
       * At this point, a new group is waiting or some group dropped off
       * To deliver next cab, let's perform next steps:
       *  - Get ordered group of waiting people by seats, the first of each category
       *  - For each waiting group, book a car, the best one keeping in mind:
       *    - A car with number of seats equals to group of people is preferrable, or the next one to avoid wasting seats
       *  - Mark the selected group as riding (to help on /locate /dropoff)
       *  - Remove selected group from waiting queue (to help on /dropoff)
       */
      const waitingGroupsByPriority = this.#groupWaitingRepository.getOrderedGroupWaitingForCarPerSeat();
      let pos = -1;
      let carAssigned = null;
      let group = null;

      while (++pos < waitingGroupsByPriority.length && !carAssigned) {
        group = waitingGroupsByPriority[pos];
        carAssigned = this.#carsAvailableRepository.assignCarForAGroup(group);
      }

      if (carAssigned) {
        // this.#logger.info(`car_assigned ${JSON.stringify(carAssigned.getDto())}`);
        this.#groupRidingRepository.addGroupIntoAcar(group, carAssigned);
        this.#groupWaitingRepository.dropOff(group.getId());
        // this.#logger.info(`journey_dispatched ${JSON.stringify(group.getDto())} ${JSON.stringify(carAssigned.getDto())}`);
      
        /**
         * Edge case
         * if we have a a car with 5 seats and is fully occupied could happen this:
         * - g1 waiting for 3 seats
         * - g2 waiting for 2 seats
         * 
         * when car release the 5 seats, g1 is assigned but still has free seats and there are people waiting
         * so it's necessary to assign a new group more into the car
         * 
         * To make a better calculus, we need to know the number of original seats and the number of occupied seats
         * in the other hand, depending on the architecture, a new message could be delivered to reassign the
         * group into the car eventually
         */
        const originalCar = this.#carsAvailableRepository.getOriginalCarById(carAssigned.getId());
        if (originalCar.getSeats().value() >= carAssigned.getSeats().value()) {
          setImmediate(() => {
            const carReleasedEvent = new CarReleasedDomainEvent(originalCar.getId());
            this.#parallelQueue.push(carReleasedEvent, defaultCallbackAfterProcessingEvent);
          });
        }
      }

      return callback(null);
    };
  }

  execute(event) {
    setImmediate(() => {
      this.#parallelQueue.push(event, defaultCallbackAfterProcessingEvent);
    });
  }
}

module.exports = JourneyDispatcher;
