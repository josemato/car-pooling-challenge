'use strict';

const CarReleasedDomainEvent = require('../../domain/events/car-released-domain-event');
const CarAvailableDomainEvent = require('../../domain/events/car-available-domain-event');
const DomainEventSubscriber = require('../../../shared/domain/bus/event/domain-event-subscriber');
const GroupWaitingCreatedDomainEvent = require('../../domain/events/group-waiting-domain-event');
const JourneyDispatcher = require('./journey-dispatcher');

class DispatchJourneyOnRidingDropOffOrNewGroup extends DomainEventSubscriber {
  /**
   * @property {JourneyDispatcher}
   */
  #dispatcher = null;

  /**
   * @property {EventBus}
   */
  #bus = null;

  /**
   * @param {JourneyDispatcher} dispatcher
   */
  constructor(dispatcher, bus) {
    super();

    this.execute = this.execute.bind(this);
    this.#dispatcher = dispatcher;
    this.#bus = bus;

    /**
     * In a future let's automate this thanks to the dependency container
     */
    this.subscribedTo().forEach(eventName => this.#bus.subscribeTo(eventName, this.execute));
  }

  subscribedTo() {
    return [
      CarReleasedDomainEvent.eventName(),
      CarAvailableDomainEvent.eventName(),
      GroupWaitingCreatedDomainEvent.eventName(),
    ];
  }

  /**
   * When a car is free for some reason (subscribedTo events) like
   * riding drop off event or a new group is waiting
   * @param {DomainEvent} event
   */
  execute(event) {
    this.#dispatcher.execute(event);
  }
}

module.exports = DispatchJourneyOnRidingDropOffOrNewGroup;
