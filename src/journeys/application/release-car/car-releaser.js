'use strict';

const CarsAvailableRepository = require('../../domain/cars-available-repository');
const CarReleasedDomainEvent = require('../../domain/events/car-released-domain-event');
const Group = require('../../domain/group');
const Car = require('../../../shared/domain/value-object/car');

class CarReleaser {
  /**
   * @property {Logger} logger
   */
  #logger = null;

  /**
   * {EventBus} bus
   */
  #bus = null;

  /**
   * @property {CarsAvailableRepository} carsAvailableRepository
   */
  #carsAvailableRepository = null;

  /**
   * @param {Logger} logger
   * @param {EventBus} bus
   * @param {CarsAvailableRepository} carsAvailableRepository 
   */
  constructor(logger, bus, carsAvailableRepository) {
    this.#logger = logger;
    this.#bus = bus;
    this.#carsAvailableRepository = carsAvailableRepository;
  }

  /**
   * We need to put this available car into the queue notifying
   * but now order is different so should wait in the end of the queue:
   *  - cabify.journey.1.event.group.riding_drop_off
   * @param {CarId} carId
   * @param {Group} group
   */
  execute(carId, group) {
    this.#carsAvailableRepository.restoreCar(carId, group);

    const events = [
      new CarReleasedDomainEvent(carId.value())
    ];
    this.#bus.publish(events);
  }
}

module.exports = CarReleaser;
