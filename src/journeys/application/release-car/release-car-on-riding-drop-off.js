'use strict';

const GroupRidingDropOffDomainEvent = require('../../domain/events/group-riding-drop-off-domain-event');
const DomainEventSubscriber = require('../../../shared/domain/bus/event/domain-event-subscriber');
const CarReleaser = require('./car-releaser');
const CarId = require('../../../shared/domain/value-object/car-id');
const Group = require('../../domain/group');

class ReleaseCarOnRidingDropOff extends DomainEventSubscriber {
  #releaser = null;

  /**
   * @param {CarReleaser} releaser
   */
  constructor(releaser, bus) {
    super();

    this.execute = this.execute.bind(this);

    this.#releaser = releaser;

    /**
     * In a future let's automate this thanks to the dependency container
     */
    this.subscribedTo().forEach(eventName => bus.subscribeTo(eventName, this.execute));
  }

  subscribedTo() {
    return [
      GroupRidingDropOffDomainEvent.eventName(),
    ];
  }

  /**
   * After a drop off, release car to be available and notify
   * when it's been available again
   * @param {GroupRidingDropOffDomainEvent} event
   */
  execute(event) {
    // we need to convert the event into a group and a car aggregator
    const {
      car: {
        id: idRaw,
      },
      group: groupDto,
    } = event.data;

    const carId = new CarId(idRaw);
    const group = Group.fromPrimitives(groupDto.id, groupDto.people, groupDto.order);


    this.#releaser.execute(carId, group);
  }
}

module.exports = ReleaseCarOnRidingDropOff;
