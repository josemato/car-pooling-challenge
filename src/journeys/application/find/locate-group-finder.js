'use strict';

const GroupId = require('../../domain/group-id');
const GroupNotFound = require('../../domain/group-not-found');
const GroupRidingRepository = require('../../domain/group-riding-repository');
const GroupWaitingRepository = require('../../domain/group-waiting-repository');

class LocateGroupFinder {
  /**
   * @property {GroupWaitingRepository}
   */
  #groupWaitingRepository = null;

  /**
   * @property {GroupRidingRepository}
   */
  #ridingRepository = null;

  /**
   * @property {CarsAvailableRepository} carsAvailableRepository
   */
  #carsAvailableRepository = null;

  /**
   * @property {EventBus} bus
   */
  #bus = null;

  /**
   * @property {Logger} logger
   */
  #logger = null;

  /**
   * @param {GroupRidingRepository} GroupRidingRepository
   * @param {GroupWaitingRepository} groupWaitingRepository
   * @param {EventBus} bus
   * @param {Logger} logger
   */
  constructor(groupWaitingRepository, ridingRepository, carsAvailableRepository, bus, logger) {
    this.#groupWaitingRepository = groupWaitingRepository;
    this.#ridingRepository = ridingRepository;
    this.#carsAvailableRepository = carsAvailableRepository;
    this.#bus = bus;
    this.#logger = logger;
  }

  /**
   * @param {Number} id
   * @returns {Car} car
   */
  async execute(idRaw) {
    const id = new GroupId(idRaw);

    const group = this.#groupWaitingRepository.getGroupWaiting(id);
    if (group) {
      return null;
    }

    const riding = this.#ridingRepository.getRidingGroup(id);
    if (riding) {
      /**
       * In this endpoint, we can return the original car created at endpoint PUT /cars
       * or we can return the car id with the available seats, both approach seems correct to me
       * Also, in my implementation, return original car has poor performance due to internal implementation I did
       *  */ 
      return this.#carsAvailableRepository.getOriginalCarById(riding.car.getId());
      // return riding.car; // this returns the car id with the available seats
    }

    throw new GroupNotFound(id);
  }
}

module.exports = LocateGroupFinder;
