'use strict';

const Group = require('../../domain/group');
const GroupId = require('../../domain/group-id');
const GroupOrder = require('../../domain/group-order');
const People = require('../../domain/people');
const GroupWaitingRepository = require('./../../domain/group-waiting-repository');
const GroupOrderGenerator = require('./../../domain/group-order-generator');

class JourneyCreator {
  #groupWaitingRepository = null;
  #ridingRepository = null;
  #groupOrderGenerator = null;
  #bus = null;
  #logger = null;

  /**
   * @param {GroupWaitingRepository} groupWaitingRepository
   * @param {GroupRidingRepository} ridingRepository
   * @param {GroupOrderGenerator} groupOrderGenerator
   * @param {EventBus} bus
   * @param {Logger} logger
   */
  constructor(groupWaitingRepository, ridingRepository, groupOrderGenerator, bus, logger) {
    this.#groupWaitingRepository = groupWaitingRepository;
    this.#ridingRepository = ridingRepository;
    this.#groupOrderGenerator = groupOrderGenerator;
    this.#bus = bus;
    this.#logger = logger;
  }

  async execute(journeyDto) {
    const { id, people: totalGroup } = journeyDto;
    const groupId = new GroupId(id);

    const car = this.#ridingRepository.getRidingGroup(groupId);
    const groupWaiting = this.#groupWaitingRepository.getGroupWaiting(groupId);

    // user was found inside a car or waiting for one, so, it's not necessary to wait
    if (car || groupWaiting) {
      return;
    }

    const people = new People(totalGroup);
    const order = new GroupOrder(this.#groupOrderGenerator.generate());
    const group = Group.create(groupId, people, order);

    this.#groupWaitingRepository.addGroupToQueue(group);

    this.#bus.publish(group.pullDomainEvents());
  }
}

module.exports = JourneyCreator;
