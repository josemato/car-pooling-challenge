'use strict';

const GroupId = require('../../domain/group-id');
const GroupWaitingRepository = require('../../domain/group-waiting-repository');
const GroupRidingRepository = require('../../domain/group-riding-repository');
const GroupNotFound = require('../../domain/group-not-found');
const GroupWaitingDropOffDomainEvent = require('../../domain/events/group-waiting-drop-off-domain-event');
const GroupRidingDropOffDomainEvent = require('../../domain/events/group-riding-drop-off-domain-event');

class GroupDropOff {
  /**
   * @property {GroupWaitingRepository}
   */
  #groupWaitingRepository = null;
  /**
   * @property {GroupRidingRepository}
   */
  #groupRidingRepository = null;

  /**
   * @property {EventBus}
   */
  #bus = null;

  /**
   * @property {Logger}
   */
  #logger = null;

  /**
   * @param {GroupWaitingRepository} groupWaitingRepository
   * @param {GroupRidingRepository} groupRidingRepository
   * @param {EventBus} bus
   * @param {Logger} logger
   */
  constructor(groupWaitingRepository, groupRidingRepository, bus, logger) {
    this.#groupWaitingRepository = groupWaitingRepository;
    this.#groupRidingRepository = groupRidingRepository;
    this.#bus = bus;
    this.#logger = logger;
  }

  /**
   * @param {GroupId} id
   * @throws {GroupNotFound}
   */
  async execute(idRaw) {
    /**
     * Check:
     *  - if group is waiting
     *  - if group is riding
     */
    const id = new GroupId(idRaw);

    /**
     * Dropoff from a riding is more efficient than drop off from priority queue
     * due to internal implementations so lets take advantage of it :)
     */
    const riding = this.#groupRidingRepository.dropOff(id);

    if (riding) {
      const {
        group,
        car,
      } = riding;

      this.#bus.publish([new GroupRidingDropOffDomainEvent(
        car.getId().value(),
        group.getId().value(),
        group.getPeople().value()
      )]);
      return;
    }

    const group = this.#groupWaitingRepository.dropOff(id);
    if (group) {
      this.#bus.publish([new GroupWaitingDropOffDomainEvent(group.getId().value(), group.getPeople().value(), group.getOrder().value())])
      return;
    }

    throw new GroupNotFound(id);
  }
}

module.exports = GroupDropOff;
