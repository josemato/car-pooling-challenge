'use strict';

const GroupRidingRepository = require('../../domain/group-riding-repository');
const GroupWaitingRepository = require('../../domain/group-waiting-repository');
const CarAvailableDomainEvent = require('../../domain/events/car-available-domain-event');
const CarsAvailableRepository = require('../../domain/cars-available-repository');

class DataResetter {
  #logger = null;
  #bus = null;
  #carsAvailableRepository = null;
  // TODO: Cars repository can be removed passing data as data event
  #carsRepository = null;
  #groupRidingRepository = null;
  #groupWaitingRepository = null;

  /**
   * @param {Logger} logger
   * @param {EventBus} bus
   * @param {CarsAvailableRepository} carsAvailableRepository
   * @param {CarsAvailableRepository} carsRepository
   * @param {GroupRidingRepository} groupRidingRepository
   * @param {GroupWaitingRepository} groupWaitingRepository
   */
  constructor(logger, bus, carsAvailableRepository, carsRepository, groupRidingRepository, groupWaitingRepository) {
    this.#logger = logger;
    this.#bus = bus;
    this.#carsAvailableRepository = carsAvailableRepository;
    this.#carsRepository = carsRepository;
    this.#groupRidingRepository = groupRidingRepository;
    this.#groupWaitingRepository = groupWaitingRepository;
  }

  execute(totalCarsAvailable) {
    this.#groupRidingRepository.clear();
    this.#groupWaitingRepository.clear();
    this.#carsAvailableRepository.generateIndex(this.#carsRepository.getCars());

    const events = [
      new CarAvailableDomainEvent(0),
    ];

    this.#bus.publish(events);
  }
}

module.exports = DataResetter;
