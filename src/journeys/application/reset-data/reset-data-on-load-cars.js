'use strict';

const CarsCreatedDomainEvent = require('../../../shared/domain/bus/event/cars-created-domain-event');
const DomainEventSubscriber = require('../../../shared/domain/bus/event/domain-event-subscriber');
const EventBus = require('../../../shared/domain/bus/event/event-bus');
const DataResetter = require('./data-resetter');

class ResetDataOnLoadCars extends DomainEventSubscriber {
  /**
   * @property {DataResetter}
   */
  #resetter = null;

  /**
   * 
   * @property {EventBus}
   */
  #bus = null;

  /**
   * @param {EventBus} bus
   * @param {CarReleaser} resetter
   */
  constructor(bus, resetter) {
    super();

    this.#bus = bus;
    this.#resetter = resetter;

    this.execute = this.execute.bind(this);

    /**
     * In a future let's automate this thanks to the dependency container
     */
    this.subscribedTo().forEach(eventName => this.#bus.subscribeTo(eventName, this.execute));
  }

  subscribedTo() {
    return [
      CarsCreatedDomainEvent.eventName(),
    ];
  }

  /**
   * After load cars, notify there are available cars
   * @param {CarsCreatedDomainEvent} event
   */
  execute(event) {
    const totalCarsAdded = event.data.total;

    this.#resetter.execute(totalCarsAdded);
  }
}

module.exports = ResetDataOnLoadCars ;
