'use strict';

/**
 * High performant hash map: https://github.com/jhuckaby/megahash
 */
const MegaHash = require('megahash');
const Group = require('../../domain/group');
const GroupId = require('../../domain/group-id');
const GroupIdCarIndexHash = require('../../domain/waiting-priority-queue-data-structures/group-id-car-index-hash');

class GroupIdCarIndexHashInMemory extends GroupIdCarIndexHash {
  #index = null;

  /**
   * Data structure to store group data by group id. We can have 1..100,000 groups in memory
   *  [group id]: [group data]
   * Example:
   * groupId1: <group1 + group>
   * groupId2: <group4 + group>
   * groupId3: <group10 + group>
   * groupId4: <group45 + group>
   * groupId5: <group15 + group>
   * groupIdN: <group23 + group>
   * 
   * This structure is optimized for:
   *  - Locate: Check if a riding group exist
   *  - Dropoff: Remove element from riding
   */
  constructor() {
    super();
    this.#index = new MegaHash();
  }

  /**
   * @param {Group} group
   * @param {Car} car
   */
  addGroup(group, car) {
    const data = {
      group: group.getDto(),
      car: car.getDto(),
    };

    const statusHash = this.#index.set(group.getId().value(), data);
    if (statusHash === 0) {
      throw new Error('GroupIdIndexHashInMemory out of memory [debug: megahash error inserting]');
    }
  }

  /**
   * @param {GroupId} id
   * @returns {Object<peopleRaw, carDto>} riding // TODO: Create a riding Aggregator
   */
  get(id) {
    const data = this.#index.get(id.value())?? null;

    if (!data) {
      return null;
    }

    return data;
  }

  /**
   * @param {GroupId} groupId
   */
  removeGroup(id) {
    this.#index.delete(id.value());
  }

  /**
   * @returns {Boolean}
   */
  isEmpty() {
    return this.#index.length() === 0;
  }

  /**
   * returns {Int}
   */
  size() {
    return this.#index.length();
  }

  clear() {
    this.#index.clear();
  }
}

module.exports = GroupIdCarIndexHashInMemory;
