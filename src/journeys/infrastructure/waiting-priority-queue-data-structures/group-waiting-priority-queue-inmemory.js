'use strict';

const LinkedList = require('dbly-linked-list');
const Group = require('../../domain/group');
const GroupWaitingPriorityQueue = require('../../domain/waiting-priority-queue-data-structures/group-waiting-priority-queue');

const getPriorityQueueKey = numberOfPeopleGroup => `seats${numberOfPeopleGroup}`;

const groupSearchComparatorByOrder = (group, groupToBeSearched) => {
  return group.getOrder().value() - groupToBeSearched.getOrder().value();
};

/**
 * Binary search adapter to work with a linked list.
 * Algorithm from https://github.com/darkskyapp/binary-search/blob/master/index.js
 * 
 * Return:
 * If the value is found in the array:
 *  it returns a nonnegative integer representing the index of the value in the array
 * If the value is not in the array:
 *  then -(index + 1) is returned, where index is where the value should be inserted into the array to maintain sorted order
 * @param {LinkedList} list
 * @param {Number} totalSize
 * @param {Node} data
 * @param {Function} comparator
 * @returns {Number} pos
 */
function binarySearch(list, listSize, needleGroup, comparator) {
  let mid = 0;
  let cmp = 0;
  let low = 0;
  let high = listSize - 1;

  while(low <= high) {
    // The naive `low + high >>> 1` could fail for array lengths > 2**31
    // because `>>>` converts its operands to int32. `low + (high - low >>> 1)`
    // works for array lengths <= 2**32-1 which is also Javascript's max array
    // length.
    mid = low + ((high - low) >>> 1);
    cmp = +comparator(list.findAt(mid).data, needleGroup, mid);

    // Too low.
    if(cmp < 0.0) {
      low  = mid + 1;
    } else if(cmp > 0.0) { // Too high.
      high = mid - 1;
    } else { // Key found.
      return mid;
    }
  }

  // Key not found.
  return ~low;
}

class GroupWaitingPriorityQueueInMemory extends GroupWaitingPriorityQueue {
  /**
   * Data structure to store groups by people (buckets). We have from 1..6 seats available at cars:
   *  [seats key]: [ordered groups by incoming time implementing a double linked list]
   * Example:
   * seats1: [<group1>, <group5>]
   * seats2: [<group4>, <group2>]
   * seats3: [<group10>, <group5>]
   * seats4: [<group45>, <group2>]
   * seats5: [<group15>, <group21>]
   * seats6: [<group23>, <group35>]
   * 
   * This structure is optimized for:
   *  - Insert group into waiting queue when request a journey
   *    - Hashmap with seats index to look for the proper queue
   *    - Priority ordered queue inserting user in the end
   *  - Get first group waiting per seat
   *    - Head of the queue
   *  - Search group on queue: Given a sorted array we can use binary search O(log n)
   */
  #priorityQueue = {
    'seats1': new LinkedList(),
    'seats2': new LinkedList(),
    'seats3': new LinkedList(),
    'seats4': new LinkedList(),
    'seats5': new LinkedList(),
    'seats6': new LinkedList(),
  };

  /**
   * Groups is between 1..6 but cars are between 4..6, so lets group in:
   *  - 1..4
   *  - 5
   *  - 6
   * @param {Group} group
   */
  addGroup(group) {
    const people = group.getPeople().value();

    const key = getPriorityQueueKey(people);

    this.#priorityQueue[key].insert(group);
  }

  /**
   * Removes a group from waiting priority queue and returns null if not exist or Group if exists
   * @param {Group} group
   * @returns {Group} null
   */
  removeGroup(group) {
    /**
     * We need to perform next actions:
     * 1. Find the group on the sortered linked list but:
     *  1.1 LinkedList is a custom implemenation so we need to create a custom comparator to use binary search
     * 2. If node is found, delete it
     */
    const pos = this.findGroupPosition(group);

    if (pos < 0) {
      return null;
    }

    const key = getPriorityQueueKey(group.getPeople().value());
    const listForSearching = this.#priorityQueue[key];
    listForSearching.removeAt(pos);

    return group;
  }

  /**
   * @returns {Int} total Number of waiting cars
   */
  size() {
    const total = Object.values(this.#priorityQueue).reduce((acc, list) => {
      return acc + list.getSize();
    }, 0);

    return total;
  }

  /**
   * Find a group keeping in mind the priority order where order is the indexed key
   * @param {Group} group
   * @returns {GroupDto} group
   */
  findGroup(group) {
    const people = group.getPeople();
    const key = getPriorityQueueKey(people.value());
    const listForSearching = this.#priorityQueue[key];
    const pos = binarySearch(listForSearching, listForSearching.getSize(), group, groupSearchComparatorByOrder);

    if (pos < 0) {
      return null;
    }

    return listForSearching.findAt(pos).data?? null;
  }

  /**
   * Group position in it's internal priority queue
   * @param {Int} group
   */
  findGroupPosition(group) {
    const people = group.getPeople();
    const key = getPriorityQueueKey(people.value());
    const listForSearching = this.#priorityQueue[key];
    const pos = binarySearch(listForSearching, listForSearching.getSize(), group, groupSearchComparatorByOrder);

    return pos;
  }

  /**
   * @param {People} people 
   * @returns {GroupDto} group
   */
  getFirstGroupByPeople(people) {
    const key = getPriorityQueueKey(people.value());
    const listForSearching = this.#priorityQueue[key];

    return listForSearching.findAt(0).data?? null;
  }

  clear() {
    Object.values(this.#priorityQueue).map(list => list.clear());
  }
}

module.exports = GroupWaitingPriorityQueueInMemory;
