'use strict';

/**
 * High performant hash map: https://github.com/jhuckaby/megahash
 */
const MegaHash = require('megahash');
const Group = require('../../domain/group');
const GroupId = require('../../domain/group-id');
const GroupIdIndexHash = require('../../domain/waiting-priority-queue-data-structures/group-id-index-hash');

class GroupIdIndexHashInMemory extends GroupIdIndexHash {
  #index = null;

  /**
   * Data structure to store group data by group id. We can have 1..100,000 groups in memory
   *  [group id]: [group data]
   * Example:
   * groupId1: <group1>
   * groupId2: <group4>
   * groupId3: <group10>
   * groupId4: <group45>
   * groupId5: <group15>
   * groupIdN: <group23>
   * 
   * This structure is optimized for:
   *  - Locate: Check if a group exist
   *  - Dropoff: Remove element
   */
  constructor() {
    super();
    this.#index = new MegaHash();
  }

  /**
   * @param {Group} group
   */
  addGroup(group) {
    const statusHash = this.#index.set(group.getId().value(), group.getDto());

    if (statusHash === 0) {
      throw new Error('GroupIdIndexHashInMemory out of memory [debug: megahash error inserting]');
    } else if (statusHash === 2) {
      throw new Error('GroupIdIndexHashInMemory overwriten and existent key');
    }
  }

  /**
   * @param {GroupId} id
   * @returns {GroupDto} groupDto
   */
  get(id) {
    const groupDto = this.#index.get(id.value())?? null;

    return groupDto;
  }

  /**
   * @param {GroupId} groupId
   * @eturns {Boolean} deleted
   */
  removeGroup(id) {
    const found = this.#index.delete(id.value());

    return found;
  }

  /**
   * @returns {Boolean}
   */
  isEmpty() {
    return this.#index.length() === 0;
  }

  /**
   * returns {Int}
   */
  size() {
    return this.#index.length();
  }

  clear() {
    this.#index.clear();
  }
}

module.exports = GroupIdIndexHashInMemory;
