'use strict';

const Megahash = require('megahash');
const AVLTree = require('avl');
const Car = require('../../../shared/domain/value-object/car');
const Seats = require('../../../shared/domain/value-object/seats');
const CarsWaitingPriorityQueue = require('../../domain/waiting-priority-queue-data-structures/cars-waiting-priority-queue');
const P = require('pino');

const getPriorityQueueKey = numberOfSeats => `seats${numberOfSeats}`;

const carSearchComparator = (car1, car2) => {
  return car1.order - car2.order;
};

/**
 * TODO
 * Add occupied attribute into car value object, by default false
 *  - return that infomation from getDto and add it into primitives event domain
 * @param {CarDto} carDto
 */
const getAttributesToSearchFor = (carDto) => {
  return {
    order: carDto.order,
  };
};

class CarsWaitingPriorityQueueInMemory extends CarsWaitingPriorityQueue {
  /**
   * Data structure to store car by seats (buckets). We have from 1..6 seats available at cars:
   *  [seats key]: [ordered cars by availability implementing a double linked list]
   * Example:
   * seats4: [<carData45>, <carData2>]
   * seats5: [<carData15>, <carData21>]
   * seats6: [<carData23>, <carData35>]
   * 
   * This structure is optimized for:
   *  - Insert car into waiting queue when car is available
   *    - Hashmap with seats index to look for the proper queue
   *    - Priority ordered queue inserting car in the end
   *  - Get first suitable car waiting per seat
   *    - Head of the queue
   *  - Search car on queue: Given a sorted array we can use binary search O(log n)
   */
  #priorityQueue = {
    'seats0': new AVLTree(carSearchComparator),
    'seats1': new AVLTree(carSearchComparator),
    'seats2': new AVLTree(carSearchComparator),
    'seats3': new AVLTree(carSearchComparator),
    'seats4': new AVLTree(carSearchComparator),
    'seats5': new AVLTree(carSearchComparator),
    'seats6': new AVLTree(carSearchComparator),
  };

  /**
   * <carId>: [{group1}, {group2}]
   */
  #indexCarWithRidingGroups = new Megahash();

  /**
   * @param {Car} car
   */
  addCar(car) {
    const seats = car.getSeats().value();
    const key = getPriorityQueueKey(seats);
    const carDto = car.getDto();

    this.#priorityQueue[key].insert(getAttributesToSearchFor(carDto), carDto);
  }

  /**
   * @param {Seats} seats
   * @returns {Car}
   */
  getFirstCarBySeats(seats) {
    const key = getPriorityQueueKey(seats.value());
    const carDto = this.#priorityQueue[key].minNode()?.data ?? null;

    if (!carDto) {
      return null;
    }

    const {
      id: idRaw,
      seats: seatsRaw,
      order: orderRaw,
    } = carDto;

    return Car.fromPrimitives(idRaw, seatsRaw, orderRaw);
  }

  /**
   * Remove all fleet
   */
  clear() {
    Object.values(this.#priorityQueue).map(list => list.destroy());
    this.#indexCarWithRidingGroups.clear();
  }

  /**
   * Given a car and a group, assign a free car to a group
   * @param {Car} car
   * @param {Group} group
   * @returns {Car} assignedCar keeping original car data
   */
  assignCarForAGroup(car, group) {
    /**
     * 1. Get groups already riding in this car to add the new group into the car index riding groups
     * 2. Remove car from current priority queue of X seats and move it into next lower priority queue of Y seats
     */
    const groupsIntoCar = this.#indexCarWithRidingGroups.get(car.getId().value())?? [];
    groupsIntoCar.push(group.getDto());
    this.#indexCarWithRidingGroups.set(car.getId().value(), groupsIntoCar);

    /**
     * Step 2
     * Remove car from current queue (seats)
     * and insert into the new one (seats - group people)
     */
    const keyCurrent = getPriorityQueueKey(car.getSeats().value())
    const carWithOccupiedSeats = new Car(
      car.getId(),
      new Seats(car.getSeats().value() - group.getPeople().value()),
      car.getOrder(),
    );

    this.#priorityQueue[keyCurrent].remove(getAttributesToSearchFor(car.getDto()));
    this.addCar(carWithOccupiedSeats);

    return carWithOccupiedSeats;
  }

  /**
   * After a drop off, restore car into the proper queue
   * @param {Car} car
   * @param {Group} group
   */
  restoreCar(car, group) {
    /**
     * Update groups into the car removing the one which dropped off
     * if number of groupsIntoCar is equal to groupsRiding then could be
     * a message duplicated and car was already restored
     */
    const groupsIntoCar = this.#indexCarWithRidingGroups.get(car.getId().value())?? [];
    const groupsRiding = groupsIntoCar.filter(g => g.id !== group.getId().value());

    /**
     * FYI: This check is not necessary for this test but in real life, queues could deliver
     * same message more than once so app needs to protect against that
     */
    if (groupsIntoCar.length === groupsRiding.length) {
      return;
    }

    this.#indexCarWithRidingGroups.set(car.getId().value(), groupsRiding);

    /**
     * Restore car seats state
     * if car finished the ridings, put a new order
     */
    const totalSeatsFilled = groupsRiding.reduce((total, g) => {
      return total + g.people;
    }, 0);

    /**
     * TODO: At this point we can decide (for fairness) if:
     *  - Car keeps the priority order after a drop off
     *  - Or recalculate its order if they finish all ridings with this group
     *    (simulating car is in the end of the queue)
     */
    const totalSeats = new Seats(car.getSeats().value() - totalSeatsFilled);
    const carWithMoreSeats = new Car(
      car.getId(),
      totalSeats,
      car.getOrder(),
    );

    /**
     * Remove previous car from old queue and insert into the new one
     */
    const totalSeatsFilledBeforeDropOff = groupsIntoCar.reduce((total, g) => {
      return total + g.people;
    }, 0);
    const previousKey = getPriorityQueueKey(totalSeatsFilledBeforeDropOff);
    this.#priorityQueue[previousKey].remove(getAttributesToSearchFor(car.getDto()));

    this.addCar(carWithMoreSeats);
  }

  /**
   * @returns {Int} total Number of available cars
   */
  size() {
    const total = Object.values(this.#priorityQueue).reduce((acc, list) => {
      return acc + list.size;
    }, 0);

    return total;
  }
}

module.exports = CarsWaitingPriorityQueueInMemory;
