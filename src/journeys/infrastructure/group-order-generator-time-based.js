'use strict';

const GroupOrder = require('../domain/group-order-generator');

class GroupOrderTimeBased extends GroupOrder {
  generate() {
    return process.hrtime.bigint();
  }
}

module.exports = GroupOrderTimeBased;
