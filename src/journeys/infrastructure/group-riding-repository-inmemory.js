'use strict';

const Car = require('../../shared/domain/value-object/car');
const Group = require('../domain/group');
const GroupRidingRepository = require('../domain/group-riding-repository');
const People = require('../domain/people');
const GroupIdCarIndexHashInMemory = require('./waiting-priority-queue-data-structures/group-id-car-index-hash-inmemory');

class GroupRidingRepositoryInMemory extends GroupRidingRepository {
  /**
   * @property {GroupIdCarIndexHashInMemory} groupsRiding
   */
  #groupsRiding = new GroupIdCarIndexHashInMemory();

  /**
   * @param {Group} group
   * @param {Car} car
   */
  addGroupIntoAcar(group, car) {
    this.#groupsRiding.addGroup(group, car);
  }

  /**
   * Check if a given group id is riding and returns the people on the given group and the car
   * @param {GroupId} id
   * @return {Riding<People, Car>} car  // TODO: create a Riding Aggregator
   */
  getRidingGroup(id) {
    const ridingDto = this.#groupsRiding.get(id);

    if (!ridingDto) {
      return null;
    }

    const {
      group: {
        id: peopleIdRaw,
        people: peopleRaw,
      },
      car: {
        id: carIdRaw,
        seats: seatsRaw,
        order: orderRaw,
      },
    } = ridingDto;

    return {
      group: Group.fromPrimitives(peopleIdRaw, peopleRaw),
      car: Car.fromPrimitives(carIdRaw, seatsRaw, orderRaw),
    };
  }

  /**
   * @param {GroupId} id
   * @returns {Riding<People, Car>} riding
   */
  dropOff(id) {
    const riding = this.getRidingGroup(id);
    if (!riding) {
      return null;
    }

    this.#groupsRiding.removeGroup(id);

    return riding;
  }

  clear() {
    this.#groupsRiding.clear();
  }

  /**
   * Return number of people who is riding
   */
  size() {
    return this.#groupsRiding.size();
  }
}

module.exports = GroupRidingRepositoryInMemory;
