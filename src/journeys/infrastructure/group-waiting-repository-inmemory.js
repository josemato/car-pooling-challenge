'use strict';

const Group = require('../domain/group');
const GroupId = require('../domain/group-id');
const People = require('../domain/people');
const GroupOrder = require('../domain/group-order');
const GroupWaitingRepository = require('../domain/group-waiting-repository');
const GroupWaitingPriorityQueueInMemory = require('./waiting-priority-queue-data-structures/group-waiting-priority-queue-inmemory');
const GroupIdIndexHashInMemory = require('./waiting-priority-queue-data-structures/group-id-index-hash-inmemory');

/**
 * This repository will be managing 2 internal data structures to improve performance:
 * 1. A Hashmap (Javascript MAP) to know if a group is waiting for a ride. Access to it is constant O(1)
 *  - An improvement could be to have a btree to search quickly but on btrees it's necessary to rebalance after
 * each insertion so I prefer a Hashmap because it has O(1) for Insert, Access, Search and Delete:
 *  https://adrianmejia.com/data-structures-time-complexity-for-beginners-arrays-hashmaps-linked-lists-stacks-queues-tutorial
 * 
 * 2. A Queue (double linked list) to insert in the end and to remove the first element quickly and to be
 * able to remove elements in the middle of the list
 *  - Improvement: Have a linkedlist with a given size to avoid asking/releasing OS memory (malloc/free)
 */
class GroupWaitingRepositoryInMemory extends GroupWaitingRepository {
  /**
   * @property {GroupWaitingPriorityQueue}
   */
  #groupWaitingPriorityQueueInMemory = null;

  /**
   * @property {GroupIdIndexHashInMemory}
   */
  #groupsWaiting = null;

  constructor() {
    super();
    this.#groupWaitingPriorityQueueInMemory = new GroupWaitingPriorityQueueInMemory();
    this.#groupsWaiting = new GroupIdIndexHashInMemory();
  }

  /**
   * @param {Group} group
   */
  addGroupToQueue(group) {
    /**
     * If group already exist for some reason at groupsWaiting, then it will trigger an error and we will not
     * save it into the priority queue. Why this order?
     *  - Because groupsWaiting is quickest than priorityQueue checking if group exist
     */
    try {
      this.#groupsWaiting.addGroup(group);
      this.#groupWaitingPriorityQueueInMemory.addGroup(group);
    } catch (e) {

    }
  }

  /**
   * @param {GroupId} id
   * @returns {Group} group
   */
  getGroupWaiting(id) {
    const groupDto = this.#groupsWaiting.get(id);

    if (!groupDto) {
      return null;
    }

    const {
      id: idRaw,
      people: peopleRaw,
      order: orderRaw,
    } = groupDto;

    return Group.fromPrimitives(idRaw, peopleRaw, orderRaw);
  }

  /**
   * @param {GroupId} id
   * @returns {Group} group
   */
  dropOff(id) {
    const group = this.getGroupWaiting(id);
    if (!group) {
      return null;
    }

    this.#groupsWaiting.removeGroup(group.getId());
    this.#groupWaitingPriorityQueueInMemory.removeGroup(group);

    return group;
  }

  /**
   * For any group with people from 1..6, get the first group of each people data
   * @returns {Array.<Group>}
   */
  getOrderedGroupWaitingForCarPerSeat() {
    const availableGroupsSortered = [
      this.#groupWaitingPriorityQueueInMemory.getFirstGroupByPeople(new People(1)),
      this.#groupWaitingPriorityQueueInMemory.getFirstGroupByPeople(new People(2)),
      this.#groupWaitingPriorityQueueInMemory.getFirstGroupByPeople(new People(3)),
      this.#groupWaitingPriorityQueueInMemory.getFirstGroupByPeople(new People(4)),
      this.#groupWaitingPriorityQueueInMemory.getFirstGroupByPeople(new People(5)),
      this.#groupWaitingPriorityQueueInMemory.getFirstGroupByPeople(new People(6)),
    ]
    .filter(group => group !== null)
    .sort((group1, group2) => {
      return group1.getOrder().value() - group2.getOrder().value();
    });

    return availableGroupsSortered;
  }

  clear() {
    this.#groupsWaiting.clear();
    this.#groupWaitingPriorityQueueInMemory.clear();
  }

  size() {
    return this.#groupsWaiting.size();
  }
}

module.exports = GroupWaitingRepositoryInMemory;
