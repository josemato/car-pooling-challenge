'use strict';

const Megahash = require('megahash');
const CarsAvailableRepository = require('../domain/cars-available-repository');
const CarsWaitingPriorityQueueInMemory = require('./waiting-priority-queue-data-structures/cars-fleet-priority-queue-inmemory');
const Car = require('../../shared/domain/value-object/car');
const Seats = require('../../shared/domain/value-object/seats');

class CarsAvailableRepositoryInMemory extends CarsAvailableRepository {
  /**
   * @property {CarsWaitingPriorityQueueInMemory}
   */
  #priorityQueue = null;

  /**
   * Structure to have all cars available by index
   */
  #indexCarById = null;

  constructor() {
    super();
    this.#priorityQueue = new CarsWaitingPriorityQueueInMemory();
    this.#indexCarById = new Megahash();

    this.addCar = this.addCar.bind(this);
  }

  /**
   * @param {Car} car
   */
  addCar(car) {
    this.#priorityQueue.addCar(car);
    this.#indexCarById.set(car.getId().value(), car.getDto());
  }

  /**
   * 
   * @param {CarId} carId
   */
  getOriginalCarById(carId) {
    const carDto = this.#indexCarById.get(carId.value())?? null;

    if (!carDto) {
      return null;
    }

    return Car.fromPrimitives(
      carDto.id,
      carDto.seats,
      carDto.order
    );
  }

  /**
   * Remove current indexed cars fleet and generate a new index with the current ones
   * @param {Array.<Car>} cars
   */
  generateIndex(cars) {
    this.#priorityQueue.clear();
    this.#indexCarById.clear();

    cars.forEach(this.addCar);
  }

  /**
   * Check if there is some available car on the fleet for the given people number
   * People can be between 1..6 but cars are from 4..6 so let's see if there is a cabify
   * to accomodate the incoming group
   * Optimized to answer with the car with less available seats to suit the group
   * Cars response will be ordered by:
   *  - Seats, from lower to higher
   *  - If seats has same number, order by arrival time (waiting time in the queue)
   * @param {People} people
   * @returns {Car} car
   */
  isThereAvailableCarForGroup(people) {
    const car = [
      this.#priorityQueue.getFirstCarBySeats(new Seats(1)),
      this.#priorityQueue.getFirstCarBySeats(new Seats(2)),
      this.#priorityQueue.getFirstCarBySeats(new Seats(3)),
      this.#priorityQueue.getFirstCarBySeats(new Seats(4)),
      this.#priorityQueue.getFirstCarBySeats(new Seats(5)),
      this.#priorityQueue.getFirstCarBySeats(new Seats(6)),
    ]
    .filter(car => car !== null)
    .find((car) => {
      if (car) {
        return car.getSeats().value() >= people.value();
      }

      return false;
    });

    return car?? null;
  }

  /**
   * Given a group, look for the best car available and assign it to it
   * @param {Group} group 
   * @returns {Car} assignedCar
   */
  assignCarForAGroup(group) {
    const people = group.getPeople();
    const car = this.isThereAvailableCarForGroup(people);

    if (!car) {
      return null;
    }

    // Assign car to a group
    return this.#priorityQueue.assignCarForAGroup(car, group);
  }

  /**
   * Dropoff action was done so car needs to add the available seats
   * @param {CarId} car
   * @param {Group} fromGroup
   */
  restoreCar(carId, fromGroup) {
    const originalCar = this.getOriginalCarById(carId);
    this.#priorityQueue.restoreCar(originalCar, fromGroup);
  }

  /**
   * @returns {Int} total Number of waiting cars
   */
  size() {
    return this.#priorityQueue.size();
  }
}

module.exports = CarsAvailableRepositoryInMemory;
