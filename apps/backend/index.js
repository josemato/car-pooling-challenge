'use strict';

require('newrelic');

const {
  ContainerBuilder,
  YamlFileLoader
} = require('node-dependency-injection');
const logger = require('pino')();
const startWebServerApp = require('./web');

/**
 * Create dependency container
 */
const container = new ContainerBuilder();
container.logger = logger;
const loader = new YamlFileLoader(container);
loader.load(`${__dirname}/services.yaml`);

container.set('service.logger', logger);
container.compile();

async function start() {
  await startWebServerApp(container);
}

start();
