'use strict';

const Ajv = require('ajv');
const fastify = require('fastify')({
  bodyLimit: 10485760, // MAX_BODY_LIMIT in bytes, 10485760 bytes = 10MB
});
const fastify405 = require('fastify-405');
const fastifyFormBody = require('fastify-formbody');
const routes = require('./routes');

const port = process.env.PORT || 9091;

fastify.register(fastifyFormBody);

/**
 * To meet cabify requirements returning 400 when body cannot be unmarshalled
 * we need to modify internal fastify ways of working (workaround)
 */
fastify.addContentTypeParser('*', function (request, payload, done) {
  done(null, null);
});

const ajv = new Ajv({
  // the fastify defaults (if needed)
  removeAdditional: false,
  useDefaults: true,
  coerceTypes: false, // we need to avoid convertir string as numbers like "5" -> 5, triggering a bad format error
  nullable: true,
});

fastify.setValidatorCompiler(({ schema, method, url, httpPart }) => {
  return ajv.compile(schema);
});

async function startWebServer(container) {
  const logger = container.get('service.logger');

  /**
   * Workaround for fastify to support http 405  method not allowed
   */
  fastify.register(fastify405, routes.healthcheck.getStatus.notAllowed);
  fastify.register(fastify405, routes.cars.loadAvailableCars(container).notAllowed);
  fastify.register(fastify405, routes.journeys.requestJourney(container).notAllowed);
  fastify.register(fastify405, routes.journeys.dropOff(container).notAllowed);
  fastify.register(fastify405, routes.journeys.locateJourney(container).notAllowed);

  fastify.route(routes.healthcheck.getStatus);
  fastify.route(routes.cars.loadAvailableCars(container));
  fastify.route(routes.journeys.requestJourney(container));
  fastify.route(routes.journeys.locateJourney(container));
  fastify.route(routes.journeys.dropOff(container));

  fastify.listen(port, '0.0.0.0', (err) => {
    if (err) {
      logger.error(err);
      process.exit(1);
    }

    logger.info(`Server up and running at port ${port}`);
  });
}

module.exports = startWebServer;
