'use strict';

const People = require('../../../../src/journeys/domain/people');

function requestJourney(container) {
  const controller = container.get('app.backend.web.journey.journey_post_controller');

  /**
   * We are using fastify ajv to validate the incoming payload
   * That approach could be good if this endpoint receives a huge payload because fastify
   * streams the payload performing the schema validation in an optimal way but, from architecture
   * perspective, this change should be validated at:
   *  - If Architecture is CQRS, when command is being created
   *  - If Architecture is Hexagonal, when use case is invoked
   * 
   * To keep this validation here, we need to know the limits of the seats.
   * This information is under domain/car/seats attribute definition model but controller should not be aware
   * of domain information. If this is an anti pattern, we can remove lines require Seats, minimun and maximum
   * validators and everything will work as expected
   */
  const router = {
    method: 'POST',
    url: '/journey',
    handler: controller.invoke,
    schema: {
      body: {
        type: 'object',
        required: ['id', 'people'],
        items: {
          properties: {
            id: {
              type: 'integer',
            },
            people: {
              type: 'integer',
              minimum: People.minPeoplePerCab(),
              maximum: People.maxPeoplePerCab(),
            },
          },
          additionalProperties: false,
        },
      },
    },
    notAllowed: {
      regexp: /\/journey.*/,
      allow: ['POST'],
    },
  };

  return router;
}

module.exports = requestJourney;
