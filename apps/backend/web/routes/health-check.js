'use strict';

const { getStatus } = require('../controller/healthcheck');

const router = {
  method: 'GET',
  url: '/status',
  handler: getStatus,
  notAllowed: {
    regexp: /\/status.*/,
    allow: ['GET'],
  },
};

module.exports = router;
