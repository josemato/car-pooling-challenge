'use strict';

const Seats = require('../../../../src/shared/domain/value-object/seats');

function loadAvailableCars(container) {
  const controller = container.get('app.backend.web.cars.cars_put_controller');

  /**
   * We are using fastify ajv to validate the incoming payload
   * That approach could be good if this endpoint receives a huge payload because fastify
   * streams the payload performing the schema validation in an optimal way but, from architecture
   * perspective, this change should be validated at:
   *  - If Architecture is CQRS, when command is being created
   *  - If Architecture is Hexagonal, when use case is invoked
   * 
   * To keep this validation here, we need to know the limits of the seats.
   * This information is under domain/car/seats attribute definition model but controller should not be aware
   * of domain information. If this is an anti pattern, we can remove lines require Seats, minimun and maximum
   * validators and everything will work as expected
   */
  const router = {
    method: 'PUT',
    url: '/cars',
    handler: controller.invoke,
    schema: {
      body: {
        type: 'array',
        items: {
          type: 'object',
          required: ['id', 'seats'],
          properties: {
            id: {
              type: 'integer',
            },
            seats: {
              type: 'integer',
              minimum: Seats.minSeatsPerCab(),
              maximum: Seats.maxSeatsPerCab(),
            },
          },
          additionalProperties: false,
        },
      },
    },
    notAllowed: {
      regexp: /\/cars.*/,
      allow: ['PUT'],
    },
  };

  return router;
}

module.exports = loadAvailableCars;
