'use strict';

const dropOff = require('./drop-off');
const getStatus = require('./health-check');
const loadAvailableCars = require('./car');
const locateJourney = require('./locate');
const requestJourney = require('./journey');

module.exports = {
  healthcheck: {
    getStatus,
  },
  cars: {
    loadAvailableCars,
  },
  journeys: {
    dropOff,
    locateJourney,
    requestJourney,
  },
};
