'use strict';

const SeatsInvalidValue = require('../../../../../src/shared/domain/value-object/seats-invalid-value');

const HTTP_CODE_BAD_REQUEST = 400;

class CarsPutController {
  #creator = null;
  #logger = null;

  /**
   * @param {CarsCreator} carsCreator
   */
  constructor(carsCreator, logger) {
    this.#creator = carsCreator;
    this.#logger = logger;

    this.invoke = this.invoke.bind(this);
  }

  async invoke(request, reply) {
    const cars = request.body;

    try {
      await this.#creator.execute(cars);
      reply.send();
    } catch (e) {
      if (e.name === 'SeatsInvalidValue') {
        return reply.code(HTTP_CODE_BAD_REQUEST).send({
          error: 'Bad Request',
          message: e.message,
        });
      }

      this.#logger.error(e);
      reply.code(500).send({
        error: 'Internal Server Error',
        message: e.message
      });
    }
  }
}

module.exports = CarsPutController;
