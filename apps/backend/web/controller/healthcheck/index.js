'use strict';

const getStatus = require('./healthcheck-get-status');

module.exports = {
  getStatus,
};
