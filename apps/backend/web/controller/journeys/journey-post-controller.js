'use strict';

const PeopleInvalidValue = require('../../../../../src/journeys/domain/people-invalid-value');

const HTTP_CODE_REQUEST_ACCEPTED = 202;
const HTTP_CODE_BAD_REQUEST = 400;

class JourneyPostController {
  #creator = null;
  #logger = null;

  /**
   * @param {JourneyCreator} journeyCreator
   * @param {Logger} logger
   */
  constructor(journeyCreator, logger) {
    this.#creator = journeyCreator;
    this.#logger = logger;

    this.invoke = this.invoke.bind(this);
  }

  async invoke(request, reply) {
    const journey = request.body;

    try {
      await this.#creator.execute(journey);

      reply.code(HTTP_CODE_REQUEST_ACCEPTED).send();
    } catch (e) {
      if (e.name === 'PeopleInvalidValue') {
        return reply.code(HTTP_CODE_BAD_REQUEST).send({
          error: 'Bad Request',
          message: e.message,
        });
      }

      if (e.name === 'GroupOrderInvalidValue') {
        return reply.code(HTTP_CODE_BAD_REQUEST).send({
          error: 'Bad Request',
          message: e.message,
          payload: request.body?? null,
        });
      }

      this.#logger.error(e);
      reply.code(500).send({
        error: 'Internal Server Error',
        message: e.message,
        payload: request.body?? null,
      });
    }
  }
}

module.exports = JourneyPostController;
