'use strict';

const GroupIdInvalidValue = require('../../../../../src/journeys/domain/people-invalid-value');
const GroupNotFound = require('../../../../../src/journeys/domain/group-not-found');

const HTTP_CODE_GROUP_RIDIDNG = 200;
const HTTP_CODE_GROUP_WAITING = 204;
const HTTP_CODE_GROUP_NOT_FOUND = 404;
const HTTP_CODE_BAD_REQUEST = 400;

class JourneyPostController {
  /**
   * @param {JourneyFinder} journeyFinder
   */
  #finder = null;
  #logger = null;

  constructor(journeyFinder, logger) {
    this.#finder = journeyFinder;
    this.#logger = logger;

    this.invoke = this.invoke.bind(this);
  }

  async invoke(request, reply) {
    try {
      const { ID: idString } = request.body;
      const id = parseInt(idString, 10);

      const car = await this.#finder.execute(id);
      if (!car) {
        return reply.code(HTTP_CODE_GROUP_WAITING).send();
      }

      reply.code(HTTP_CODE_GROUP_RIDIDNG).send(car.getDto());
    } catch (e) {
      if (e.name === 'GroupIdInvalidValue') {
        return reply.code(HTTP_CODE_BAD_REQUEST).send({
          error: 'Bad Request',
          message: e.message,
        });
      }

      if (e.name === 'GroupNotFound') {
        return reply.code(HTTP_CODE_GROUP_NOT_FOUND).send();
      }

      this.#logger.error(e);
      reply.code(500).send({
        error: 'Internal Server Error',
        message: e.message
      });
    }
  }
}

module.exports = JourneyPostController;
