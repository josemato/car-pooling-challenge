'use strict';

const GroupDropoff = require('../../../../../src/journeys/application/create/group-drop-off');
const GroupIdInvalidValue = require('../../../../../src/journeys/domain/group-id-invalid-value');
const GroupNotFound = require('../../../../../src/journeys/domain/group-not-found');

const HTTP_CODE_GROUP_UNREGISTERED = 204;
const HTTP_CODE_GROUP_NOT_FOUND = 404;
const HTTP_CODE_BAD_REQUEST = 400;

/*
200 OK or 204 No Content When the group is unregistered correctly.
404 Not Found When the group is not to be found.
400 Bad Request When there is a failure in the request format or the
payload can't be unmarshalled.
*/

class DropOffPostController {
  #dropOff = null;
  #logger = null;

  /**
   * @param {GroupDropoff} groupDropOff
   */
  constructor(dropOff, logger) {
    this.#dropOff = dropOff;
    this.#logger = logger;

    this.invoke = this.invoke.bind(this);
  }

  async invoke(request, reply) {
    try {
      const { ID: id } = request.body;
      await this.#dropOff.execute(id);

      reply.code(HTTP_CODE_GROUP_UNREGISTERED).send();
    } catch (e) {
      if (e.name === 'GroupNotFound') {
        return reply.code(HTTP_CODE_GROUP_NOT_FOUND).send();
      }

      if (e.name === 'GroupIdInvalidValue') {
        return reply.code(HTTP_CODE_BAD_REQUEST).send({
          error: 'Bad Request',
          message: e.message,
        });
      }

      this.#logger.error(e);
      reply.code(500).send({
        error: 'Internal Server Error',
        message: e.message
      });
    }
  }
}

module.exports = DropOffPostController;
