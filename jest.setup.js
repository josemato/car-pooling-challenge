'use strict';

const {
  ContainerBuilder,
  YamlFileLoader
} = require('node-dependency-injection');
const logger = require('pino')();

const container = new ContainerBuilder();
container.logger = logger;
const loader = new YamlFileLoader(container);
loader.load(`${__dirname}/apps/backend/services.yaml`);

container.set('service.logger', logger);
container.compile();

module.exports = () => {
  global.__CONTAINER__ = container;
};
