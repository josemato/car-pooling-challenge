Feature: Car with lower seats is assigned to groups
  In order to optimize ridings
  As a user business
  I want to deliver cars with lest seats to user

  Scenario: A fresh environment with 3 cars
    Given I send a PUT request to "/cars" with "json" body:
    """
    [{
      "id": 1,
      "seats": 6
    }, {
      "id": 2,
      "seats": 4
    }, {
      "id": 3,
      "seats": 6
    }]
    """

  Scenario: Three groups asking for a group
    Given I send a POST request to "/journey" with "json" body:
    """
    {
      "id": 1,
      "people": 4
    }
    """
    Then the response status code should be 202
    Then the response should be empty
    Given I send a POST request to "/locate" with "urlencoded" body:
    """
    ID=1
    """
    Then the response status code should be 200
    Then the response body should be my car:
    """"
    {
      "id": 2,
      "seats": 4
    }
    """