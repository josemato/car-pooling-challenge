'use strict';

// features/riding/steps.js
const { 
  Given,
  Then,
} = require('cucumber');
const requester = require('../../requester');
const expect = require('unexpected');

Then('the response body should be my car:', async function (body) {
  const response = await requester.getLastResponse();
  const carShouldBe = JSON.parse(body);
  const carFromBackend = response.data;

  expect(carFromBackend, 'to satisfy', carShouldBe);
});
