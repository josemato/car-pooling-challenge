Feature: Available cars are being assigned in order of group priority and seats requested
  In order to optimize ridings
  As a user business
  I want to deliver cars with lest seats to user

  Scenario: A fresh environment
    Given I send a PUT request to "/cars" with "json" body:
    """
    [{
      "id": 1,
      "seats": 4
    }]
    """

    Given I send a POST request to "/journey" with "json" body:
    """
    {
      "id": 1,
      "people": 4
    }
    """
    Then the response status code should be 202

    Given I send a POST request to "/journey" with "json" body:
    """
    {
      "id": 2,
      "people": 5
    }
    """
    Then the response status code should be 202

    Given I send a POST request to "/journey" with "json" body:
    """
    {
      "id": 3,
      "people": 2
    }
    """
    Then the response status code should be 202


    Given I send a POST request to "/dropoff" with "urlencoded" body:
    """
    ID=1
    """
    Then the response status code should be 204

    # g2 needs 5 seats and just 4 are available, so g3 should ride
    Given I send a POST request to "/locate" with "urlencoded" body:
    """
    ID=2
    """
    Then the response status code should be 204

    Given I send a POST request to "/locate" with "urlencoded" body:
    """
    ID=3
    """
    Then the response body should be my car:
    """"
    {
      "id": 1,
      "seats": 4
    }
    """
