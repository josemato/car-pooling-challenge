Feature: Car with lower seats is assigned to groups with different people size
  In order to optimize ridings
  As a user business
  I want to deliver cars with lest seats to user

  Scenario: A fresh environment
    Given I send a PUT request to "/cars" with "json" body:
    """
    [{
      "id": 1,
      "seats": 6
    }]
    """

    Given I send a POST request to "/journey" with "json" body:
    """
    {
      "id": 1,
      "people": 3
    }
    """
    Then the response status code should be 202

    Given I send a POST request to "/journey" with "json" body:
    """
    {
      "id": 2,
      "people": 1
    }
    """
    Then the response status code should be 202

    Given I send a POST request to "/journey" with "json" body:
    """
    {
      "id": 3,
      "people": 2
    }
    """
    Then the response status code should be 202

    Given I send a POST request to "/locate" with "urlencoded" body:
    """
    ID=1
    """
    Then the response body should be my car:
    """"
    {
      "id": 1,
      "seats": 6
    }
    """

    Given I send a POST request to "/locate" with "urlencoded" body:
    """
    ID=2
    """
    Then the response body should be my car:
    """"
    {
      "id": 1,
      "seats": 6
    }
    """

    Given I send a POST request to "/locate" with "urlencoded" body:
    """
    ID=3
    """
    Then the response body should be my car:
    """"
    {
      "id": 1,
      "seats": 6
    }
    """