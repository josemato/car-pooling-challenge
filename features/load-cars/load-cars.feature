Feature: Load cars
  In order to have cars in the platform
  As a user with admin permissions
  I want to load new cars

  Scenario: A valid non existent car
    Given I send a PUT request to "/cars" with "json" body:
    """
    [{
      "id": 1,
      "seats": 6
    }]
    """
    Then the response status code should be 200
    And the response should be empty
