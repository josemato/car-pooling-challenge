Feature: Group request a drop off after a journey because there are no available cars
  In order to keep waiting
  As a usser
  I want to drop off

  Scenario: A fresh environment
    Given I send a PUT request to "/cars" with "json" body:
    """
    []
    """

    Given I send a POST request to "/journey" with "json" body:
    """
    {
      "id": 1,
      "people": 1
    }
    """
    Then the response status code should be 202
    Then the response should be empty

    Given I send a POST request to "/locate" with "urlencoded" body:
    """
    ID=1
    """
    Then the response status code should be 204
    Then the response should be empty

    Given I send a POST request to "/dropoff" with "urlencoded" body:
    """
    ID=1
    """
    Then the response status code should be 204
    Then the response should be empty

    Given I send a POST request to "/locate" with "urlencoded" body:
    """
    ID=1
    """
    Then the response status code should be 404
    Then the response should be empty