Feature: Group request a drop off but there is not journey requested previously
  In order to detect non users
  As a Cabify
  I want to return Group Not Found

  Scenario: A fresh environment
    Given I send a PUT request to "/cars" with "json" body:
    """
    []
    """

    Given I send a POST request to "/dropoff" with "urlencoded" body:
    """
    ID=100
    """
    Then the response status code should be 404
    Then the response should be empty
