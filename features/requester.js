'use strict';

const axios = require('axios');

// The port number that the Server is listening on
const port = process.env.PORT || 60926; // || 9091;

// The Base URL to the server, before our request-specific path is added
const baseUrl = `http://localhost:${port}`;

console.log(baseUrl);

// The last response that we received
let lastResponse;

function formatDataFromHeader(body, contentType) {
  if (contentType === 'application/json') {
    const jsonBody = JSON.parse(body);

    return jsonBody;
  }

  return body;
}

/**
 * 
 * @param {String} url
 * @param {String} data
 */
async function doPutRequest(path, body, contentType) {
  const url = `${baseUrl}${path}`;

  const axiosConfig = {
    url,
    method: 'PUT',
    data: formatDataFromHeader(body, contentType),
    headers: {
      'content-type': contentType,
    },
  };

  lastResponse = await axios(axiosConfig);
}

async function doPostRequest(path, body, contentType) {
  const url = `${baseUrl}${path}`;

  const axiosConfig = {
    url,
    method: 'POST',
    data: formatDataFromHeader(body, contentType),
    headers: {
      'content-type': contentType,
    },
  };

  try {
    lastResponse = await axios(axiosConfig);
  } catch (e) {
    lastResponse = e.response;
  }
}

async function getLastResponse() {
  return lastResponse;
}

module.exports = {
  doPutRequest,
  doPostRequest,
  getLastResponse,
};
