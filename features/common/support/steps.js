'use strict';

// features/common/support/steps.js
const { 
  Given,
  Then,
} = require('cucumber');
const requester = require('../../requester');
const expect = require('unexpected');

const formatToHttp = {
  'json': 'application/json',
  'urlencoded': 'application/x-www-form-urlencoded',
};

Given('I send a PUT request to {string} with {string} body:', function(path, format, body) {
  return requester.doPutRequest(path, body, formatToHttp[format]);
});

Given('I send a POST request to {string} with {string} body:', function(path, format, body) {
  return requester.doPostRequest(path, body, formatToHttp[format]);
});

Then('the response status code should be {int}', async function(statusCode) {
  const response = await requester.getLastResponse();

  expect(response.status, 'to equal', statusCode);
});

Then('the response should be empty', async function () {
  const response = await requester.getLastResponse();

  expect(response.data, 'to equal', '');
});

Then('the response should be:', async function (body) {
  const response = await requester.getLastResponse();
  const shouldBe = JSON.parse(body);
  const dataFromBackend = response.data;

  expect(dataFromBackend, 'to satisfy', shouldBe);
});
