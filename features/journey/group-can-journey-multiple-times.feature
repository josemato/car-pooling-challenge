Feature: Group request a journey multiple times and there is no errors
  In order to keep waiting
  As a usser
  I want to drop off

  Scenario: A fresh environment
    Given I send a PUT request to "/cars" with "json" body:
    """
    [{
      "id": 1,
      "seats": 4
    }]
    """

    Given I send a POST request to "/journey" with "json" body:
    """
    {
      "id": 1,
      "people": 1
    }
    """
    Then the response status code should be 202
    Then the response should be empty

    Given I send a POST request to "/locate" with "urlencoded" body:
    """
    ID=1
    """
    Then the response should be:
    """
    {
      "id": 1,
      "seats": 4
    }
    """