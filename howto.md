# How To

In this section let's take a look about how requirements were achieved

## Current Requirements

* The service should be as efficient as possible.
* It should be able to work reasonably well with at least 10<sup>4</sup> / 10<sup>5</sup> cars / waiting groups.

## Use cases

### Load the list of available cars in the service and remove all previous data

This method may be called more than once during the life cycle of the service.

Each time this use case is being invoked, cars are being loaded into the system. A car has between 4 and 6 seats when it's added but can ride groups between 1 and 6 persons.

Knowing a group of people is composed by 1, 2, 3, 4, 5 or 6 members and they can request a journey, an optimal way to search cars is splitting the data (sharding / bucket) creating and index like:

[key] -> [value]

Where key is the index denoting the number of seats per car and value is a data structure containing all the cars with the given seats (later we will see the car structure), having:

* seats 1 -> [cars with 1 seat]
* seats 2 -> [cars with 1 seat]
* seats 3 -> [cars with 1 seat]
* seats 4 -> [cars with 1 seat]
* seats 5 -> [cars with 1 seat]
* seats 6 -> [cars with 1 seat]

This structure was implemented as a `hashmap`.

And know let's see how to search cars in a quick way. To do that, there are many valid Tree structures that could work perfectly fine but I decided to implement an `AVL Tree` because:
* Better support for Node.js: https://www.npmjs.com/package/avl There was other alternatives but this API was more complete and simple
* When info fits in memory it seems performance is a bit better than btree for searching

This structure will help us to:
* Search the best car for a given group
* When a car is assigned, remove it from the current AVL Tree and insert it into the new one (current seats - occupied ones)
* Restore a car when a group is being dropped off using same process, removing it from current AVL Tree and inserting it into the new one (current seats + released ones)

With all this info explained, this is a piece of the code with the current implementation

```
#priorityQueue = {
    'seats0': new AVLTree(carSearchComparator),
    'seats1': new AVLTree(carSearchComparator),
    'seats2': new AVLTree(carSearchComparator),
    'seats3': new AVLTree(carSearchComparator),
    'seats4': new AVLTree(carSearchComparator),
    'seats5': new AVLTree(carSearchComparator),
    'seats6': new AVLTree(carSearchComparator),
  };
```

What it was not mentioned is the use of index seats0, as you can figure out, it is were cars fully occupied are being stored.

**This action triggers next domain events:**
* CarsCreatedDomainEvent: System will regenerate cars index and will delete journey (waiting / riding) data

### A group of people requests to perform a journey

When a group of people wants to perform a journey, they are always being assigned into a `priority waiting queue`. During this process, groups are being given an order (based on a current timestamp with micro seconds).

This `priority waiting queue` is also sharded by number of people, or like we are going to find it into the code, number of requested seats. Group of people is ordered into a queue implementing a double linked list (used library [https://www.npmjs.com/package/dbly-linked-list](https://www.npmjs.com/package/dbly-linked-list))

With this structure we can:
* Insert a group in the waiting queue in order
  * Inserting in the end when group is being created
* Look in order for all available groups waiting for a car (to be assigned later into a free car and see how is the best matcher)
* When a group is assigned into a car, remove the group (first node in the double linked list)
* Remove any group (during drop off use case) using binary search because all groups are ordered

Let's see a piece of the code with this implementation:
```
/**
* Data structure to store groups by people (buckets). We have from 1..6 seats available at cars:
*  [seats key]: [ordered groups by incoming time implementing a double linked list]
* Example:
* seats1: [<group1>, <group5>]
* seats2: [<group4>, <group2>]
* seats3: [<group10>, <group5>]
* seats4: [<group45>, <group2>]
* seats5: [<group15>, <group21>]
* seats6: [<group23>, <group35>]
* 
* This structure is optimized for:
*  - Insert group into waiting queue when request a journey
*    - Hashmap with seats index to look for the proper queue
*    - Priority ordered queue inserting user in the end
*  - Get first group waiting per seat
*    - Head of the queue
*  - Search group on queue: Given a sorted array we can use binary search O(log n)
**/
#priorityQueue = {
  'seats1': new LinkedList(),
  'seats2': new LinkedList(),
  'seats3': new LinkedList(),
  'seats4': new LinkedList(),
  'seats5': new LinkedList(),
  'seats6': new LinkedList(),
};

 /**
   * Auxiliry data structure used intenally to help on restore car after a journey,
   * to know what groups were riding into a car. This structure is not being exposed to the outside.
   * <carId>: [{group1}, {group2}]
   */
  #indexCarWithRidingGroups = new Megahash();
```

When an user is assigned into a car, we will store a reference into a hashmap: `group id car riding hashmap`. The library used was development on C++ to have a better performance: [https://www.npmjs.com/package/megahash](https://www.npmjs.com/package/megahash)

There is also an extra auxiliary data, `group waiting hashmap`, to help on searching to answer next question "Given a group id, is it waiting?". This can be useful for location to avoid iterating the previous sortedlinked list. But to be honest, this extra data structure needs to be maintained on each insertion/deletion so would be useful if from business perspective endpoint /journey and endpoint /location will be hitted with frequency.

**This action triggers next domain events:**
* GroupWaitingCreatedDomainEvent: System will try to find a car for this new waiting user

### Locate a group
Given a group ID such that ID=X, return the car the group is traveling with, or no car if they are still waiting to be served.

For this case, there is no complex stuff because we have a direct access to check group id exist at `group id car riding hasmap` `group waiting hasmap` defined previously. Access has complexity O(1) accessing to each hashmap and checking in which one the user exist or not/


### A group of people requests to be dropped off. Whether they traveled or not.

An user can drop off whenever they want and this will remove the user from the `waiting queue` and `group waiting hashmap` or from the `group id car riding hashmap`. 

**This action triggers next domain events:**
* GroupRidingDropOffDomainEvent: If the user was riding and after triggering this event car will be restored (releasing seats)
* GroupWaitingDropOffDomainEvent: If the user was waiting. The system is not using it despite is being triggered.


## Architecture

This application was done following next principles:
* I tried to follow Domain Driven Design but unfortunately it is not fully done on that methodology :/
* Clean Architecture was followed but can be improved. From my perspective there are couple things that could be done better
* Keeping in mind the requirements, "load cars" could receive max 100,000 cars data. A car data is an object like {"id": 100000, "seats": 5} where it has aprox 23 characters length, so:
  * 23 characteres * 100,000 max cars data = 2300000 bytes = 2,2 MB (aprox) that needs to be handle. To avoid framework limitations, this endpoint is accepting 10MB

To dispatch the cars in order and to avoid concurrency problems, there is just 1 worker looking for the cars and assigning them to waiting groups.

![One worker dispatching the cars](/docs/images/architecture-to-avoid-concurrency.jpeg "Architecture to avoid concurrency")


## Improvements
* Follow DDD
  * Add more methods into each value object to allow comparisons betweeen then without retrieving primitive values
  * Move all car domain definition into shared context
  * Add an infrastructure implementation for the sequential queue which is dispatching the journeys
* Decouple components enhacing Event Bus passing messages
* Maybe CQRS for /journey (creating a journey command handler) and /locate (creating a query handler)
* Use ATDD since the beginning [https://intelygenz.com/blog/what-is-atdd/](https://intelygenz.com/blog/what-is-atdd/) and gain more experience on bdd
* Create AsyncApi documentation [https://www.asyncapi.com/](https://www.asyncapi.com/)