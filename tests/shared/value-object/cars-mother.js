'use strict';

const CarsCollection = require('../../../src/cars/domain/cars'); // TODO: move cars into shared :(
const CarMother = require('./car-mother');
const SeatsMother = require('./seats-mother');

class CarsCollectionMother {
  /**
   * @param {Array<Car>} cars
   * @returns {CarsCollection}
   */
  static create(cars) {
    const carsCollection = new CarsCollection(cars);

    return carsCollection;
  }

  static random(size = 10) {
    const cars = [];
    for (let i = 0; i < size; i+=1) {
      cars.push(CarMother.random());
    }

    return CarsCollectionMother.create(cars);
  }

  static randomWithSeats(seatsRaw, size = 10) {
    const cars = [];

    for (let i = 0; i < size; i+=1) {
      cars.push(CarMother.randomWithGivenSeats(seatsRaw));
    }

    return CarsCollectionMother.create(cars);
  }
}

module.exports = CarsCollectionMother;
