'use strict';

const CarId = require('../../../src/shared/domain/value-object/car-id');

/**
 * To guarantee some unique ids, faker library random is not enough so
 * let's generate them auto incrementally during testing
 */
let autoIncrementId = 0;

class CarIdMother {
  /**
   * @param {Number} id
   */
  static create(id) {
    return new CarId(id);
  }

  static random() {
    const id = ++ autoIncrementId;
    return CarIdMother.create(id);
  }
}

module.exports = CarIdMother;
