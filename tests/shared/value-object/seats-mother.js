'use strict';

const faker = require('faker');
const Seats = require('../../../src/shared/domain/value-object/seats');

class SeatsMother {
  static create(total) {
    return new Seats(total);
  }

  static random() {
    const total = faker.random.number({
      min: Seats.minSeatsPerCab(),
      max: Seats.maxSeatsPerCab(),
    });

    return SeatsMother.create(total);
  }
}

module.exports = SeatsMother;
