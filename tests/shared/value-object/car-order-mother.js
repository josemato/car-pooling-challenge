'use strict';

const CarOrder = require('../../../src/shared/domain/value-object/car-order');
const CarOrderGeneratorTimeBased = require('../../../src/shared/infrastructure/car-order-generator-time-based');

const generator = new CarOrderGeneratorTimeBased();

class GroupOrderMother {
  /**
   * @param {Int} order
   */
  static create(order) {
    return new CarOrder(order);
  }

  static random() {
    return GroupOrderMother.create(generator.generate());
  }
}

module.exports = GroupOrderMother;
