'use strict';

const CarIdMother = require('./car-id-mother');
const SeatsMother = require('./seats-mother');
const CarOrderMother = require('./car-order-mother');
const Car = require('../../../src/shared/domain/value-object/car');
const CarId = require('../../../src/shared/domain/value-object/car-id');
const CarOrder = require('../../../src/shared/domain/value-object/car-order');

class CarMother {
  /**
   * @param {CarId} id
   * @param {Seats} seats
   * @param {CarOrder} order
   */
  static create(id, seats, order) {
    return new Car(id, seats, order);
  }

  static random() {
    const id = CarIdMother.random();
    const seats = SeatsMother.random();
    const order = CarOrderMother.random();

    return CarMother.create(id, seats, order);
  }

  static randomWithGivenSeats(seatsRaw) {
    const id = CarIdMother.random();
    const seats = SeatsMother.create(seatsRaw);
    const order = CarOrderMother.random();

    return CarMother.create(id, seats, order);
  }
}

module.exports = CarMother;
