'use strict';

const GroupOrder = require('../../../src/journeys/domain/group-order');
const GroupOrderGeneratorTimeBased = require('../../../src/journeys/infrastructure/group-order-generator-time-based');

const generator = new GroupOrderGeneratorTimeBased();

class GroupOrderMother {
  /**
   * @param {Int} order
   */
  static create(order) {
    return new GroupOrder(order);
  }

  static random() {
    return GroupOrderMother.create(generator.generate());
  }
}

module.exports = GroupOrderMother;
