'use strict';

const GroupId = require('../../../src/journeys/domain/group-id');

/**
 * To guarantee some unique ids, random is not enough so
 * let's generate them auto incrementally during testing
 */
let autoIncrementId = 0;

class GroupIdMother {
  /**
   * @param {Number} id
   */
  static create(id) {
    return new GroupId(id);
  }

  static random() {
    const id = ++ autoIncrementId;
    return GroupIdMother.create(id);
  }
}

module.exports = GroupIdMother;
