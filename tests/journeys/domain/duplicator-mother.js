'use strict';

const _ = require('lodash');

class DuplicatorMother {
  static clone(object) {
    return _.cloneDeep(object);
  }
}

module.exports = DuplicatorMother;
