'use strict';

const faker = require('faker');
const People = require('../../../src/journeys/domain/people');

class PeopleMother {
  static create(total) {
    return new People(total);    
  }

  static random() {
    const total = faker.random.number({
      min: People.minPeoplePerCab(),
      max: People.maxPeoplePerCab(),
    });

    return PeopleMother.create(total);
  }
}

module.exports = PeopleMother;
