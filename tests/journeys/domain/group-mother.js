'use strict';

const GroupIdMother = require('./group-id-mother');
const GroupOrderMother = require('./group-order-mother');
const PeopleMother = require('./people-mother');
const Group = require('../../../src/journeys/domain/group');
const GroupOrder = require('../../../src/journeys/domain/group-order');

class GroupMother {
  /**
   * @param {GroupId} id
   * @param {People} people
   * @param {GroupOrder}
   */
  static create(id, people, order) {
    return new Group(id, people, order);
  }

  static random() {
    const id = GroupIdMother.random();
    const people = PeopleMother.random();
    const order = GroupOrderMother.random();

    return GroupMother.create(id, people, order);
  }
}

module.exports = GroupMother;
