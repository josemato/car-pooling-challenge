'use strict';

const {
  describe,
  expect,
  test
} =  require('@jest/globals');

const GroupMother = require('../domain/group-mother');
const GroupWaitingRepositoryInMemory = require('../../../src/journeys/infrastructure/group-waiting-repository-inmemory');
const GroupIdMother = require('../domain/group-id-mother');
const PeopleMother = require('../domain/people-mother');
const GroupOrderMother = require('../domain/group-order-mother');
const GroupOrderGeneratorTimeBased = require('../../../src/journeys/infrastructure/group-order-generator-time-based');

describe('GroupWaitingRepositoryInMemory', () => {
  let groupWaitingRepository = null;
  let orderGenerator = new GroupOrderGeneratorTimeBased();

  beforeEach(async () => {
    groupWaitingRepository = new GroupWaitingRepositoryInMemory();
  });

  afterEach(() => {
    groupWaitingRepository.clear();
  });

  test('group_not_found', () => {
    // Arrange
    const group1 = GroupMother.random();

    // Act
    const group = groupWaitingRepository.getGroupWaiting(group1.getId());

    // Assert
    expect(group).toBe(null);
  });

  test('group_found_at_waiting_queue', () => {
    // Arrange
    const group1 = GroupMother.random();
    groupWaitingRepository.addGroupToQueue(group1);

    // Act
    const groupFound = groupWaitingRepository.getGroupWaiting(group1.getId());

    // Assert
    expect(group1).toEqual(groupFound);
  });

  test('group_not_found_with_one_member_created', () => {
    // Arrange
    const existentGroup = GroupMother.random();
    const nonExistentGroup = GroupMother.random();
    groupWaitingRepository.addGroupToQueue(existentGroup);

    // Act
    const groupFound = groupWaitingRepository.getGroupWaiting(nonExistentGroup.getId());

    // Assert
    expect(groupFound).toBe(null);
  });

  test('dropoff_from_inexistent_group_successful', () => {
    // Arrange
    const nonExistentGroup = GroupMother.random();

    // Act
    const groupFound = groupWaitingRepository.dropOff(nonExistentGroup.getId());

    // Assert
    expect(groupFound).toBe(null);
  });

  test('dropoff_existent_group_successful', () => {
    // Arrange
    const existentGroup = GroupMother.random();
    groupWaitingRepository.addGroupToQueue(existentGroup);

    // Act
    const groupFound = groupWaitingRepository.dropOff(existentGroup.getId());

    // Assert
    expect(groupFound).toEqual(existentGroup);
  });

  test('existent_groups_are_well_ordered_when_all_groups_are_full', () => {
    const groups = [
      GroupMother.create(GroupIdMother.random(), PeopleMother.create(1), GroupOrderMother.create(orderGenerator.generate())),
      GroupMother.create(GroupIdMother.random(), PeopleMother.create(5), GroupOrderMother.create(orderGenerator.generate())),
      GroupMother.create(GroupIdMother.random(), PeopleMother.create(6), GroupOrderMother.create(orderGenerator.generate())),
      GroupMother.create(GroupIdMother.random(), PeopleMother.create(2), GroupOrderMother.create(orderGenerator.generate())),
      GroupMother.create(GroupIdMother.random(), PeopleMother.create(3), GroupOrderMother.create(orderGenerator.generate())),
      GroupMother.create(GroupIdMother.random(), PeopleMother.create(4), GroupOrderMother.create(orderGenerator.generate())),
    ];

    groups.forEach((group) => groupWaitingRepository.addGroupToQueue(group));

    // Act
    const orderedGroups = groupWaitingRepository.getOrderedGroupWaitingForCarPerSeat();

    // Assert
    expect(orderedGroups.length).toBe(groups.length);
    expect(orderedGroups[0].getPeople().value()).toEqual(groups[0].getPeople().value());
    expect(orderedGroups[1].getPeople().value()).toEqual(groups[1].getPeople().value());
    expect(orderedGroups[2].getPeople().value()).toEqual(groups[2].getPeople().value());
    expect(orderedGroups[3].getPeople().value()).toEqual(groups[3].getPeople().value());
    expect(orderedGroups[4].getPeople().value()).toEqual(groups[4].getPeople().value());
    expect(orderedGroups[5].getPeople().value()).toEqual(groups[5].getPeople().value());
  });

  test('no_existent_groups_so_empty_array_is_expected', () => {
    // Arrange

    // Act
    const orderedGroups = groupWaitingRepository.getOrderedGroupWaitingForCarPerSeat();

    // Assert
    expect(orderedGroups.length).toBe(0);
  });

  test('existent_groups_are_well_ordered_when_groups_are_not_full', () => {
    // Arrange
    const groups = [
      GroupMother.create(GroupIdMother.random(), PeopleMother.create(1), GroupOrderMother.create(orderGenerator.generate())),
      GroupMother.create(GroupIdMother.random(), PeopleMother.create(6), GroupOrderMother.create(orderGenerator.generate())),
      GroupMother.create(GroupIdMother.random(), PeopleMother.create(5), GroupOrderMother.create(orderGenerator.generate())),
    ];

    groups.forEach((group) => groupWaitingRepository.addGroupToQueue(group));

    // Act
    const orderedGroups = groupWaitingRepository.getOrderedGroupWaitingForCarPerSeat();

    // Assert
    expect(orderedGroups.length).toBe(groups.length);
    expect(orderedGroups[0].getPeople().value()).toEqual(groups[0].getPeople().value());
    expect(orderedGroups[1].getPeople().value()).toEqual(groups[1].getPeople().value());
    expect(orderedGroups[2].getPeople().value()).toEqual(groups[2].getPeople().value());
  });
});
