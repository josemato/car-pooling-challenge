'use strict';

const {
  describe,
  expect,
  test
} =  require('@jest/globals');

const Group = require('../../../src/journeys/domain/group');
const GroupMother = require('../domain/group-mother');
const GroupIdIndexHash = require('../../../src/journeys/domain/waiting-priority-queue-data-structures/group-id-index-hash');
const GroupIdIndexHashInMemory = require('../../../src/journeys/infrastructure/waiting-priority-queue-data-structures/group-id-index-hash-inmemory');

describe('group_waiting_index_hash', () => {
  /**
   * @property {GroupIdIndexHash}
   */
  let groupIdIndexHash = null;

  beforeEach(() => {
    groupIdIndexHash = new GroupIdIndexHashInMemory();
  });

  test('group_index_hash_is_empty', () => {
    // Act
    const isEmpty = groupIdIndexHash.isEmpty();

    // Assert
    expect(isEmpty).toBe(true);
  });

  test('group_index_hash_is_full', () => {
    // Arrange
    for (let i = 0; i < Group.maxWaitingGroups(); i+=1) {
      const group = GroupMother.random();
      groupIdIndexHash.addGroup(group);
    }

    // Act
    const size = groupIdIndexHash.size();

    // Assert
    expect(size).toEqual(Group.maxWaitingGroups());
  });

  test('group_index_hash_is_empty_after_being_full', () => {
    // Arrange
    const groupIdsToBeDeleted = new Array(Group.maxWaitingGroups);
    for (let i = 0; i < Group.maxWaitingGroups(); i+=1) {
      const group = GroupMother.random();
      groupIdIndexHash.addGroup(group);
      groupIdsToBeDeleted[i] = group.getId();
    }

    for (let i = 0; i < Group.maxWaitingGroups(); i+=1) {
      groupIdIndexHash.removeGroup(groupIdsToBeDeleted[i]);
    }

    // Act
    const size = groupIdIndexHash.size();

    // Assert
    expect(size).toEqual(0);
  });
});
