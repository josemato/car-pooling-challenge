'use strict';

/**
 * Temporal use to test if order number is being unique or not
 */
const fs = require('fs');

const {
  describe,
  expect,
  test
} =  require('@jest/globals');
const faker = require('faker');
const DuplicatorMother = require('../domain/duplicator-mother');
const Group = require('../../../src/journeys/domain/group');
const GroupMother = require('../domain/group-mother');
const GroupWaitingPriorityQueueInMemory = require('../../../src/journeys/infrastructure/waiting-priority-queue-data-structures/group-waiting-priority-queue-inmemory');
const GroupId = require('../../../src/journeys/domain/group-id');
const People = require('../../../src/journeys/domain/people');
const GroupOrder = require('../../../src/journeys/domain/group-order');

const NS_PER_MS = 1e6;

describe('group_waiting_queue_in_memory_implementation', () => {
  /**
   * @property {GroupWaitingPriorityQueueInMemory}
   */
  let priorityQueue = null;

  beforeEach(() => {
    priorityQueue = new GroupWaitingPriorityQueueInMemory();
  });

  afterEach(() => {
    priorityQueue = null;
  });

  test('test_queue_is_empty', () => {
    // Act
    const totalGroupsWaiting = priorityQueue.size();

    // Assert
    expect(totalGroupsWaiting).toBe(0);
  });

  test('test_group_is_being_added_into_priority_queue', () => {
    // Arrange
    const group = GroupMother.random();

    // Act
    priorityQueue.addGroup(group);
    const totalGroupsWaiting = priorityQueue.size();

    // Assert
    expect(totalGroupsWaiting).toBe(1);
  });

  test('test_priority_queue_can_store_max_waiting_groups', () => {
     // Arrange
     for (let i = 0; i < Group.maxWaitingGroups(); i+=1) {
      const group = GroupMother.random();
      priorityQueue.addGroup(group);
    }

    // Act
    const totalGroupsWaiting = priorityQueue.size();

    // Assert
    expect(totalGroupsWaiting).toBe(Group.maxWaitingGroups());
  });

  test('test_priority_when_full_can_calculate_size_less_10_ms', () => {
    // Arrange
    for (let i = 0; i < Group.maxWaitingGroups(); i+=1) {
      const group = GroupMother.random();
      priorityQueue.addGroup(group);
    }

    // Act
    const start = process.hrtime();
    const totalGroupsWaiting = priorityQueue.size();
    const [, totalTimeSizeOperation] = process.hrtime(start);

    // diff nanoseconds between start and end
    const threshold = 10 * NS_PER_MS; // ms to nanoseconds

    // Assert
    expect(totalTimeSizeOperation).toBeLessThan(threshold);
  });

  test('test_find_group_when_queue_is_full', () => {
    const randomPosToSearch = faker.random.number({
      min: 0,
      max: Group.maxWaitingGroups() - 1,
    });

    let groupToSearchFor = null;

    // const fixtures = [];

    for (let i = 0; i < Group.maxWaitingGroups(); i+=1) {
      const group = GroupMother.random();

      
      priorityQueue.addGroup(group);
      // fixtures.push(group.getDto());

      if (i === randomPosToSearch) {
        groupToSearchFor = DuplicatorMother.clone(group);
      }
    }

    // fs.writeFileSync(`${process.cwd()}/debug_linked_list/fixtures_test_find_group_when_queue_is_full.json`, JSON.stringify(fixtures, null, 2));

    // Act
    const groupFound = priorityQueue.findGroup(DuplicatorMother.clone(groupToSearchFor));

    // Assert
    expect(groupFound).toEqual(groupToSearchFor);
  });

  test('test_remove_group_when_queue_is_full', () => {
    const randomPosToSearch = faker.random.number({
      min: 0,
      max: Group.maxWaitingGroups() - 1,
    });

    let groupToSearchFor = null;

    // Arrange
    for (let i = 0; i < Group.maxWaitingGroups(); i+=1) {
      const group = GroupMother.random();
      priorityQueue.addGroup(group);

      if (i === randomPosToSearch) {
        groupToSearchFor = DuplicatorMother.clone(group);
      }
    }

    // Act
    const groupFound = priorityQueue.findGroup(groupToSearchFor);
    priorityQueue.removeGroup(groupFound);
    const groupNotFound = priorityQueue.findGroup(groupToSearchFor);

    expect(groupFound).toEqual(groupToSearchFor);
    expect(groupNotFound).toEqual(null);
  });
});
