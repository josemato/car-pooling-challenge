'use strict';

const {
  describe,
  expect,
  test
} =  require('@jest/globals');

const CarMother = require('../../shared/value-object/car-mother');
const GroupMother = require('../domain/group-mother');
const LocateApplicationService = require('../../../src/journeys/application/find/locate-group-finder');
const GroupWaitingRepositoryInMemory = require('../../../src/journeys/infrastructure/group-waiting-repository-inmemory');
const RidingRepositoryInMemory = require('../../../src/journeys/infrastructure/group-riding-repository-inmemory');
const GroupNotFound = require('../../../src/journeys/domain/group-not-found');
const CarsAvailableRepositoryInMemory = require('../../../src/journeys/infrastructure/cars-available-repository-inmemory');

describe('use_case_group_locate', () => {
  let locate = null;
  let groupWaitingRepository = null;
  let carsAvailableRepository = null;
  let ridingRepository = null;

  beforeEach(() => {
    groupWaitingRepository = new GroupWaitingRepositoryInMemory();
    ridingRepository = new RidingRepositoryInMemory();
    carsAvailableRepository = new CarsAvailableRepositoryInMemory();

    locate = new LocateApplicationService(groupWaitingRepository, ridingRepository, carsAvailableRepository);
  });

  afterEach(() => {
    locate = null;
  });

  test('group_exist_on_waiting_queue_and_locate_is_successful', async () => {
    // Arrange
    const group = GroupMother.random();

    // Act
    groupWaitingRepository.addGroupToQueue(group);
    const result = await locate.execute(group.getId().value());

    // Assert
    expect(result).toBeNull();
  });

  test('group_not_exist_so_group_not_found_exception_must_be_triggered', async () => {
    // Arrange
    const group = GroupMother.random();

    // Act + Assert
    await expect(locate.execute(group.getId().value())).rejects.toThrow(GroupNotFound);
  });
});
