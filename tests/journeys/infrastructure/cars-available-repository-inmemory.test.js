'use strict';

const {
  describe,
  expect,
  test,
  beforeEach,
  afterEach,
} = require('@jest/globals');

const CarsCollectionMother = require('../../shared/value-object/cars-mother');
const CarMother = require('../../shared/value-object/car-mother');
const CarsAvailableRepositoryInMemory = require('../../../src/journeys/infrastructure/cars-available-repository-inmemory');
const PeopleMother = require('../domain/people-mother');
const Seats = require('../../../src/shared/domain/value-object/seats');
const People = require('../../../src/journeys/domain/people');

describe('cars_available_repository_in_memory', () => {
  /**
   * @property {CarsAvailableRepositoryInMemory}
   */
  let availableCarsRepository = null;

  beforeEach(() => {
    availableCarsRepository = new CarsAvailableRepositoryInMemory();
  });

  afterEach(() => {
    availableCarsRepository = null;
  });

  test('car_is_added_into_priority_queue', () => {
    // Arrange
    const car = CarMother.random();

    // Act
    const sizeBeforeInserting = availableCarsRepository.size();
    availableCarsRepository.addCar(car);
    const sizeAfterInserting = availableCarsRepository.size();

    // Assert (we could check if same node exist instead of calculating size)
    expect(sizeBeforeInserting).toBe(0);
    expect(sizeAfterInserting).toBe(1);
  });

  test('given_car_is_not_the_first_one_in_the_queue_because_queue_is_empty', () => {
    // Arrange
    const car1 = CarMother.randomWithGivenSeats(5);

    // Act
    const assignedCar = availableCarsRepository.isThereAvailableCarForGroup(new People(car1.getSeats().value()));
    const availableCarsTotal = availableCarsRepository.size();

    // Assert (we could check if same node exist instead of calculating size)
    expect(assignedCar).toBe(null);
    expect(availableCarsTotal).toBe(0);
  });

  test('given_car_is_the_first_one_in_the_queue', () => {
    // Arrange
    const car1 = CarMother.randomWithGivenSeats(6);
    const car2 = CarMother.randomWithGivenSeats(6);

    availableCarsRepository.addCar(car1);
    availableCarsRepository.addCar(car2);

    // Act
    const availableCar = availableCarsRepository.isThereAvailableCarForGroup(car1.getSeats());
    const sizeAfterInserting = availableCarsRepository.size();

    // Assert (we could check if same node exist instead of calculating size)
    expect(availableCar).toEqual(car1);
    expect(sizeAfterInserting).toBe(2);
  });

  test('cars_fleet_is_being_deleted', () => {
    // Arrange
    const car1 = CarMother.randomWithGivenSeats(5);
    const car2 = CarMother.randomWithGivenSeats(4);

    availableCarsRepository.addCar(car1);
    availableCarsRepository.addCar(car2);

    // Act
    const sizeBeforeDeletion = availableCarsRepository.size();
    availableCarsRepository.generateIndex(CarsCollectionMother.random(0).getCars());
    const sizeAfterDeletion = availableCarsRepository.size();

    // Assert (we could check if same node exist instead of calculating size)
    expect(sizeBeforeDeletion).toBe(2);
    expect(sizeAfterDeletion).toBe(0);
  });

  test('cars_fleet_is_being_generated_removing_previous_fleet', () => {
    // Arrange
    const car1 = CarMother.randomWithGivenSeats(5);
    const car2 = CarMother.randomWithGivenSeats(4);
    const carsCollection = CarsCollectionMother.random(3);

    availableCarsRepository.addCar(car1);
    availableCarsRepository.addCar(car2);

    // Act
    const sizeBeforeDeletion = availableCarsRepository.size();
    availableCarsRepository.generateIndex(carsCollection.getCars());
    const sizeAfterInsertion = availableCarsRepository.size();

    // Assert (we could check if same node exist instead of calculating size)
    expect(sizeBeforeDeletion).toBe(2);
    expect(sizeAfterInsertion).toBe(3);
  });

  test('there_is_available_car_for_3_people', () => {
    // Arrange
    const people = PeopleMother.create(3);
    const car1 = CarMother.randomWithGivenSeats(5);
    const car2 = CarMother.randomWithGivenSeats(4);
    const car3 = CarMother.randomWithGivenSeats(6);

    availableCarsRepository.addCar(car1);
    availableCarsRepository.addCar(car2);
    availableCarsRepository.addCar(car3);

    // Act
    const recommendedCar = availableCarsRepository.isThereAvailableCarForGroup(people);

    // Assert (we could check if same node exist instead of calculating size)
    expect(recommendedCar).toEqual(car2);
  });

  test('there_is_available_car_for_4_people', () => {
    // Arrange
    const people = PeopleMother.create(4);
    const car1 = CarMother.randomWithGivenSeats(5);
    const car2 = CarMother.randomWithGivenSeats(4);
    const car3 = CarMother.randomWithGivenSeats(6);

    availableCarsRepository.addCar(car1);
    availableCarsRepository.addCar(car2);
    availableCarsRepository.addCar(car3);

    // Act
    const recommendedCar = availableCarsRepository.isThereAvailableCarForGroup(people);

    // Assert (we could check if same node exist instead of calculating size)
    expect(recommendedCar).toEqual(car2);
  });

  test('there_is_available_car_for_5_people', () => {
    // Arrange
    const people = PeopleMother.create(5);
    const car1 = CarMother.randomWithGivenSeats(5);
    const car2 = CarMother.randomWithGivenSeats(4);
    const car3 = CarMother.randomWithGivenSeats(6);

    availableCarsRepository.addCar(car1);
    availableCarsRepository.addCar(car2);
    availableCarsRepository.addCar(car3);

    // Act
    const recommendedCar = availableCarsRepository.isThereAvailableCarForGroup(people);

    // Assert (we could check if same node exist instead of calculating size)
    expect(recommendedCar).toEqual(car1);
  });

  test('there_is_available_car_for_6_people', () => {
    // Arrange
    const people = PeopleMother.create(6);
    const car1 = CarMother.randomWithGivenSeats(5);
    const car2 = CarMother.randomWithGivenSeats(4);
    const car3 = CarMother.randomWithGivenSeats(6);

    availableCarsRepository.addCar(car1);
    availableCarsRepository.addCar(car2);
    availableCarsRepository.addCar(car3);

    // Act
    const recommendedCar = availableCarsRepository.isThereAvailableCarForGroup(people);

    // Assert (we could check if same node exist instead of calculating size)
    expect(recommendedCar).toEqual(car3);
  });
});
