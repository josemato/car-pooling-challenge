'use strict';

const {
  describe,
  expect,
  test
} = require('@jest/globals');
const Group = require('../../../src/journeys/domain/group');
const Car = require('../../../src/shared/domain/value-object/car');
const GroupRidingRepositoryInMemory = require('../../../src/journeys/infrastructure/group-riding-repository-inmemory');
const GroupMother = require('../domain/group-mother');
const CarMother = require('../../shared/value-object/car-mother');

describe('group_riding_repository_in_memory', () => {
  /**
   * @property {GroupRidingRepositoryInMemory}
   */
  let repository = null;

  beforeEach(() => {
    repository = new GroupRidingRepositoryInMemory();
  });

  afterEach(() => {
    repository = null;
  });

  test('test_riding_is_added', () => {
    // Arrange
    const group = GroupMother.random();
    const car = CarMother.random();

    // Act
    repository.addGroupIntoAcar(group, car);
    const { car: groupFoundInCar } = repository.getRidingGroup(group.getId());

    // Assert
    expect(groupFoundInCar).toEqual(car);
  });

  test('test_dropoff_from_existent_group', () => {
    // Arrange
    const group = GroupMother.random();
    const car = CarMother.random();

    // Act
    repository.addGroupIntoAcar(group, car);
    const { car: carFoundOnDropOff } = repository.dropOff(group.getId());
    const carNotFoundSecondDropOff = repository.dropOff(group.getId());

    // Assert
    expect(carFoundOnDropOff).toEqual(car);
    expect(carNotFoundSecondDropOff).toEqual(null);
  });

  test('test_a_car_is_returned_after_multiple_get_riding_group_calls', () => {
    // Arrange
    const group = GroupMother.random();
    const car = CarMother.random();

    // Act
    repository.addGroupIntoAcar(group, car);
    const { car: groupFoundInCar1 } = repository.getRidingGroup(group.getId());
    const { car: groupFoundInCar2 } = repository.getRidingGroup(group.getId());
    const { car: groupFoundInCar3 } = repository.getRidingGroup(group.getId());

    // Assert
    expect(groupFoundInCar1).toEqual(car);
    expect(groupFoundInCar2).toEqual(car);
    expect(groupFoundInCar3).toEqual(car);
  });

  test('test_available_cars_are_0_when_clear_is_requested', () => {
    // Arrange
    const group = GroupMother.random();
    const car = CarMother.random();

    // Act
    repository.addGroupIntoAcar(group, car);
    const sizeBeforeDeletion = repository.size();
    repository.clear();
    const sizeAfterDeletion = repository.size();

    // Assert
    expect(sizeBeforeDeletion).toEqual(1);
    expect(sizeAfterDeletion).toEqual(0);
  });
});
