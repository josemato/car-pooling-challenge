'use strict';

const {
  describe,
  expect,
  test
} =  require('@jest/globals');

const EventBus = require('../../../src/shared/infrastructure/bus/event/in-memory/event-bus-in-memory');
const CarMother = require('../../shared/value-object/car-mother');
const GroupMother = require('../domain/group-mother');
const DropOffApplicationService = require('../../../src/journeys/application/create/group-drop-off');
const GroupWaitingRepositoryInMemory = require('../../../src/journeys/infrastructure/group-waiting-repository-inmemory');
const RidingRepositoryInMemory = require('../../../src/journeys/infrastructure/group-riding-repository-inmemory');
const GroupNotFound = require('../../../src/journeys/domain/group-not-found');

// mock event bus
// jest.mock('../../../src/shared/infrastructure/bus/event/in-memory/event-bus-in-memory');

describe('use_case_group_drop_off', () => {
  let dropOff = null;
  let groupWaitingRepository = null;
  let ridingRepository = null;
  let eventBus = new EventBus(console);

  beforeEach(() => {
    groupWaitingRepository = new GroupWaitingRepositoryInMemory();
    ridingRepository = new RidingRepositoryInMemory();

    dropOff = new DropOffApplicationService(groupWaitingRepository, ridingRepository, eventBus);
  });

  afterEach(() => {
    dropOff = null;
  });

  test('group_exist_on_waiting_queue_and_dropoff_is_successful', async () => {
    // Arrange
    const group = GroupMother.random();

    // Act
    groupWaitingRepository.addGroupToQueue(group);
    await dropOff.execute(group.getId().value());

    // Assert
    // No checks. If there is no exception, drop off was fine
  });

  test('group_not_exist_so_group_not_found_exception_must_be_triggered', async () => {
    // Arrange
    const group = GroupMother.random();

    // Act + Assert
    await expect(dropOff.execute(group.getId().value())).rejects.toThrow(GroupNotFound);
  });

  test('group_is_riding_and_dropoff_is_successful', async () => {
    // Arrange
    const group = GroupMother.random();
    const car = CarMother.random();

    // Act
    ridingRepository.addGroupIntoAcar(group, car);
    await dropOff.execute(group.getId().value());

    // Assert
    // No checks. If there is no exception, drop off was fine
  });
});
