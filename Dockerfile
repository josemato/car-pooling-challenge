FROM node:14.16.0

# This Dockerfile is optimized for go binaries, change it as much as necessary
# for your language of choice.

# RUN apk --no-cache add ca-certificates=20190108-r0 libc6-compat=1.1.19-r10

EXPOSE 9091

# COPY car-pooling-challenge /
WORKDIR /usr/src/car-polling-challenge
COPY . .

#RUN apk --no-cache add --virtual native-deps \
#  g++ gcc libgcc libstdc++ linux-headers make python && \
#  npm install --quiet node-gyp -g &&\
#  npm install --quiet && \
 # apk del native-deps

RUN npm install --production


 
# ENTRYPOINT [ "/car-pooling-challenge" ]
CMD ["npm", "run", "start"]
# ENTRYPOINT [ "/bin/ping", "-c" ]
# CMD ["localhost"]
